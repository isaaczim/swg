﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory;
using RPG.Inventory.Containers;
using RPG.Inventory.Inventory;

namespace UI
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ReadKey();
            LookInBag();
            Console.ReadLine();
            
            
        }

        static void LookInBag()
        {
            MainInventory _myInventory = new MainInventory();
            _myInventory.AddItem(new Backpack(), 1);
            _myInventory.DisplayContents();
        }
    }
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RPG.Inventory.Containers;
using RPG.Inventory.Weapons;

namespace RPG.Inventory.tests
{
    [TestFixture]
    class BackpackTests
    {
        [Test]
        public void putItemInBackpackSuccsess()
        {
            //Arrange
            Backpack pack = new Backpack();
            Boomerang rang = new Boomerang();
            WoodenSword sword = new WoodenSword();
            BattleAxe axe = new BattleAxe();

            //Act
            pack.AddItem(rang,1);
            pack.AddItem(axe,20);
            pack.AddItem(sword,4);

            //Assert
            Assert.AreEqual(2, pack.ItemCount);

            pack.DisplayContents();
        }
    }
}

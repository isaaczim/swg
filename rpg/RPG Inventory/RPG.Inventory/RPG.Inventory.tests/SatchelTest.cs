﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Internal;
using RPG.Inventory.Containers;
using RPG.Inventory.Weapons;

namespace RPG.Inventory.tests
{
    [TestFixture]
    public class SatchelTest
    {
        [Test]
        public void canAddLittleItems()
        {
            Satchel bag = new Satchel();
            Boomerang rang = new Boomerang();
            WoodenSword sword = new WoodenSword();

            bag.AddItem(rang,0);
            bag.AddItem(sword,1);

            Assert.AreEqual(2,bag.ItemCount);

            bag.DisplayContents();
        }

        [Test]
        public void CannotAddBigItems()
        {
            Satchel bag = new Satchel();
            WoodenSword sword = new WoodenSword();
            BattleAxe axe = new BattleAxe();

            bag.AddItem(sword,0);
            bag.AddItem(axe,1);

            Assert.AreEqual(1,bag.ItemCount);

            bag.DisplayContents();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RPG.Inventory.Containers;
using RPG.Inventory.Reagents;
using RPG.Inventory.Weapons;

namespace RPG.Inventory.tests
{
    [TestFixture]
    class ReagentPouchTests
    {
        [Test]
        public void canAddReagent()
        {
            ReagentPouch bag = new ReagentPouch();
            Mushroom shroom = new Mushroom();

            bag.AddItem(shroom,0);

            Assert.AreEqual(1,bag.ItemCount);

            bag.DisplayContents();
        }

        [Test]
        public void AddInvalidItem()
        {
            ReagentPouch bag = new ReagentPouch();
            Mushroom shroom = new Mushroom();
            BattleAxe axe = new BattleAxe();

            bag.AddItem(shroom,0);
            bag.AddItem(axe,1);

            Assert.AreEqual(1,bag.ItemCount);

            bag.DisplayContents();
        }
    }
}

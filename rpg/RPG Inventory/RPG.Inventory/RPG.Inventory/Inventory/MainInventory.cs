﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;
using RPG.Inventory.Containers;

namespace RPG.Inventory.Inventory
{
    public class MainInventory : Container
    {
        public MainInventory() : base(4)
        {
            Name = "The Main Inventory Screen";
            Description = string.Format($"Where you can find all our the items on your person.");
            Weight = 5;
            Value = 10000000;
            Type = ItemType.Container;
        }

    }
}

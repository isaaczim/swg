﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Inventory.Base
{
    public abstract class Container : Item
    {
        protected int _capacity;
        protected int _currentIndex;
        protected Dictionary<int, Item> _itemsInBag;

        public Container(int capacity)
        {
            _capacity = capacity;
            _itemsInBag = new Dictionary<int, Item>();
            _currentIndex = 0;
        }

        public virtual void AddItem(Item itemToAdd, int position)
        {
            if(_currentIndex >= _capacity)
                Console.WriteLine("This bag is already full.");
            else if (!_itemsInBag.ContainsKey(position))
            {
                _itemsInBag.Add(position,itemToAdd);
                _currentIndex++;
            }
            else
                Console.WriteLine("That spot is already has something in it.");
        }

        public virtual Item RemoveItem(int position)
        {
            if (_itemsInBag.ContainsKey(position))
            {
                Item itemToReturn = _itemsInBag[position];
                _itemsInBag[position] = null;
                _currentIndex--;
                return itemToReturn;
            }
            else
                Console.WriteLine("There's nothing there.");
            return null;
        }

        public virtual void DisplayContents()
        {
            Console.WriteLine("Items in bag: ");

            foreach (Item item in _itemsInBag.Values)
            {
                if (item != null)
                {
                    Console.WriteLine($"\t{item.Name} - {item.Description}");
                }
            }
        }

        public int ItemCount { get { return _currentIndex; } }
    }
}

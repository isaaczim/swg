﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Containers
{
    public class Backpack : Container
    {
        public Backpack() : base(2)
        {
            Name = "A leather backpack";
            Description = string.Format($"This is a {_capacity} slot backpack.");
            Weight = 5;
            Value = 1500;
            Type = ItemType.Container;

        }
    }
}

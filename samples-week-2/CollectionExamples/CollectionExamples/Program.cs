﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionExamples
{
    class Program
    {
        static void Main(string[] args)
        {
            //ShowArrayList();
            //ShowHashTable();
            //ShowStack();
            //ShowQueue();
            //ShowGenericList();
            ShowDictionary();
        }

        static void ShowArrayList()
        {
            ArrayList intList = new ArrayList();

            intList.Add(1);
            intList.Add(5);
            intList.Add(10);
            intList.Add(4);

            int sum = 0;

            foreach (object o in intList)
            {
                sum += (int)o;
            }

            Console.WriteLine("The Sum of the arraylist is {0}",sum );
            Console.ReadKey();
            Console.Clear();
        }

        private static void ShowHashTable()
        {
            Hashtable applicationMap = new Hashtable();

            applicationMap.Add("txt", "notepad.exe");
            applicationMap.Add("bmp", "paint");
            applicationMap.Add("jpg", "paint.exe");
            applicationMap.Add("docx", "Word.exe");

            Console.WriteLine("Enter and extension: ");
            string extension = Console.ReadLine();

            Console.WriteLine("We would open {0} with {1}", extension, applicationMap[extension]);

            Console.WriteLine("\nEnter a new Extension: ");
            extension = Console.ReadLine();

            Console.WriteLine("Enter a new app: ");
            string application = Console.ReadLine();

            if (applicationMap.Contains(extension))
            {
                Console.WriteLine("Key already exists.");
            }
            else
            {
                applicationMap.Add(extension,application);
            }

            Console.WriteLine("\nPrintAll Values\n____________");
            foreach (var key in applicationMap.Keys)
            {
                Console.WriteLine(applicationMap[key]);
            }


            Console.ReadLine();
            Console.Clear();
        }

        private static void ShowStack()
        {
            Stack myStack = new Stack();
            myStack.Push("Hello");
            myStack.Push("World");  
            myStack.Push("!");

            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine(myStack.Pop());
            }
            Console.ReadLine();
        }

        static void ShowQueue()
        {
            Queue myQueue = new Queue();
            myQueue.Enqueue("Hello ");
            myQueue.Enqueue("World");
            myQueue.Enqueue("!");

            for (int i = 0; i < 3; i++)
            {
                Console.Write(myQueue.Dequeue());
            }

            Console.ReadLine();
        }

        static void ShowGenericList()
        {
            List<Person> people = new List<Person>()
            {
                new Person { FirstName = "Isaac", LastName = "Zimmerman", Age = 21 },
                new Person { FirstName = "Fred", LastName = "Flinsone", Age = 3200 }
            };

            foreach (var person in people)
            {
                Console.WriteLine("{0}, {1} is {2} years old.", person.LastName,person.FirstName,person.Age);
            }

            Console.WriteLine("\nEnter a first name: ");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter a last name: ");
            string lastname = Console.ReadLine();
            Console.WriteLine("Enter an age: ");
            int age = int.Parse(Console.ReadLine());

            people.Add(new Person {FirstName = firstName, LastName = lastname, Age = age});

            foreach (var person in people)
            {
                Console.WriteLine("{0}, {1} is {2} years old.", person.LastName, person.FirstName, person.Age);
            }

            Console.ReadLine();
        }

        static void ShowDictionary()
        {
            Dictionary<int, Person> people = new Dictionary<int, Person>();

            Person p1 = new Person() {FirstName = "Homer", LastName = "Simpson", Age = 42, PersonId = 1};
            Person p2 = new Person() {FirstName = "Marge", LastName = "Simpson", Age = 43, PersonId = 2};
            Person p3 = new Person() {FirstName = "Lisa", Age = 12, LastName = "Simpson", PersonId = 3};
            Person p4 = new Person() {FirstName = "Bart", LastName = "Simpson", Age = 9, PersonId = 4};

            people.Add(p1.PersonId, p1);
            people.Add(p2.PersonId, p2);
            people.Add(p3.PersonId, p3);
            people.Add(p4.PersonId, p4);

            foreach (var key in people.Keys)
            {
                PrintPerson(people[key]);
            }

            Console.WriteLine("\n\n => {0}{1}{2}", people[3].FirstName,people[3].LastName,people[3].Age);

            Console.ReadLine();


        }

        static void PrintPerson(Person p)
        {
            Console.WriteLine("{0} {1} {2}", p.FirstName,p.LastName,p.Age );

        }
    }
}

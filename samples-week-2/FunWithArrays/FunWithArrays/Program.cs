﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithArrays
{
    class Program
    {
        static void Main(string[] args)
        {
            SimpleArrays();
            ArryInitialization();
            DeclareImplicitArray();
            ArrayOfObjects();
            RectMultiArray();
        }

        static void SimpleArrays()
        {
            Console.WriteLine("***** Simple Array *****");

            int[] myInts = new int[3];
            myInts[0] = 100;
            myInts[1] = 200;
            myInts[2] = 300;

            foreach (int nums in myInts)
            {
                Console.WriteLine("{0}",nums);
            }
            Console.ReadKey();
        }

        static void ArryInitialization()
        {
            Console.Clear();
            Console.WriteLine("********* Array Initialization *********");

            string[] stringArray = new string[] {"one","two","three"};
            Console.WriteLine("stringArray has {0} elements", stringArray.Length);

            bool[] boolArray = {false, true, false};
            Console.WriteLine("boolArray has {0} elements", boolArray.Length);

            int[] intArray = new int[4] {20,22,23,15};
            Console.WriteLine($"intArray has{intArray.Length} elements");

            Console.ReadKey();
        }

        static void DeclareImplicitArray()
        {
            Console.Clear();
            Console.WriteLine("******** Implicit Array Initialization *********");

            var a = new[] {1, 10, 100, 1000};
            Console.WriteLine($"a is a: {a.ToString()}");

            var b = new[] {1, 1.5, 2, 25};
            Console.WriteLine($"b is a: {b.ToString()}");

            var c = new[] {"hello", "howdy"};
            Console.WriteLine($"c is a: {c.ToString()}");

            Console.ReadKey();
        }

        static void ArrayOfObjects()
        {
            Console.Clear();
            Console.WriteLine("********* Object Array *********");

            object[] myObjects = new object[4];
            myObjects[0] = 10;
            myObjects[1] = false;
            myObjects[2] = new DateTime(1969, 2,24);
            myObjects[3] = "somesting in an object array";

            foreach (object myObject in myObjects)
            {
                Console.WriteLine($"type: {myObject.GetType()}. It contains {myObject}");
            }

            Console.ReadKey();
        }

        static void RectMultiArray()
        {
            Console.Clear();
            Console.WriteLine(@"_/**\_/*-._.-*\_/**\_2D Array_/**\_/*-._.-*\_/**\_");

            int[,] myMaytrix;
            myMaytrix = new int[6,6];

            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    myMaytrix[i, j] = i*j;
                }
            }

            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    Console.Write($"{myMaytrix[i, j]}\t");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}

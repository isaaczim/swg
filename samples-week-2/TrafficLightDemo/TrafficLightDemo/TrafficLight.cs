﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficLightDemo
{
    public class TrafficLight
    {
        public TrafficLightColor CurrentColor { get; private set; }

        public TrafficLight()
        {
            CurrentColor = TrafficLightColor.Red;

        }

        public void ChangeColor()
        {
            switch (CurrentColor)
            {
                case TrafficLightColor.Red:
                    CurrentColor = TrafficLightColor.Green;
                    break;
                case TrafficLightColor.Green:
                    CurrentColor = TrafficLightColor.Yellow;
                    break;
                default:
                    CurrentColor = TrafficLightColor.Red;
                    break;
            }
        }

    }
}

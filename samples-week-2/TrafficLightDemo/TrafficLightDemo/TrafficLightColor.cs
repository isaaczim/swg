﻿namespace TrafficLightDemo
{
    public enum TrafficLightColor
    {
        Red,
        Yellow,
        Green
    }
}

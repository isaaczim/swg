﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    public class Exercises
    {

        public void Ex01()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Where(p => p.UnitsInStock == 0);

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }

        public void Ex02()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Where(p => p.UnitsInStock >= 0 && p.UnitPrice >= 3);

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }

        public void Ex03()
        {
            var customers = DataLoader.LoadCustomers();

            var results = customers.Where(c => c.Region == "WA");

            foreach (var customer in results)
            {
                Console.WriteLine($"Name: {customer.CompanyName}\nOrders:");
                foreach (var order in customer.Orders)
                {
                    Console.WriteLine($"\t{order.OrderID}");
                }
            }
        }

        public void Ex04()
        {
            var products = DataLoader.LoadProducts();

            var results = from product in products
                select product.ProductName;

            foreach (var p in results)
            {
                Console.WriteLine(p);
            }
        }

        public void Ex05()
        {
            var products = DataLoader.LoadProducts();

            var results = from product in products
                select new {product.ProductName, product.UnitPrice};

            foreach (var p in results)
            {
                Console.WriteLine($"{p.ProductName}, Original Price: {p.UnitPrice}, Increased Price: {(double)(p.UnitPrice)*1.25}\n");
            }

        }

        public void Ex06()
        {
            var products = DataLoader.LoadProducts();

            var results = from product in products
                          select product.ProductName;

            foreach (var p in results)
            {
                Console.WriteLine(p.ToUpper());
            }
        }

        public void Ex07()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Where(p => p.UnitsInStock%2 == 0);

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }

        public void Ex08()
        {
            var products = DataLoader.LoadProducts();

            var results = from product in products
                select new {product.ProductName, product.Category, Price = product.UnitPrice};

            foreach (var p in results)
            {
                Console.WriteLine($"{p.ProductName}: \n\t{p.Category}, Price: {p.Price}");
            }
        }

        public void Ex09()
        {
            Console.WriteLine("Not Implemented");
        }

        public void Ex10()
        {
            var customers = DataLoader.LoadCustomers();

            var products = DataLoader.LoadProducts();

            var results = from customer in customers
                select new {customer.Orders, customer.CustomerID};

            foreach (var result in results)
            {
                Console.WriteLine($"{result.CustomerID}:");
                foreach (var order in result.Orders)
                {
                    if (order.Total <500)
                    {
                        Console.WriteLine($"\n ID: {order.OrderID}, Total: {order.Total}");
                    }
                }
                Console.WriteLine();
            }

        }

        public void Ex11()
        {
            var numsA = DataLoader.NumbersA;

            var result = numsA.Take(3);

            foreach (var i in result)
            {
                Console.WriteLine(i);
            }
        }

        public void Ex12()
        {
            var customers = DataLoader.LoadCustomers();

            var results = from c in customers
                where c.Region == "WA"
                select new {orders = c.Orders.Take(3), c.CustomerID};

            foreach (var result in results)
            {
                Console.WriteLine($"{result.CustomerID}: \n");
                foreach (var order in result.orders)
                {
                    Console.WriteLine($"\t{order.OrderDate}\n" +
                                      $"\t{order.OrderID}\n" +
                                      $"\t{order.Total.ToString("c")}\n");
                }
            }
        }

        public void Ex13()
        {
            var numsA = DataLoader.NumbersA;

            var results = numsA.Skip(3);

            foreach (var result in results)
            {
                Console.WriteLine(result  + " ");
            }
        }

        public void Ex14()
        {
            var customers = DataLoader.LoadCustomers();

            var results = from c in customers
                where c.Region == "WA"
                select new {orders = c.Orders.Skip(2), c.CustomerID};

            foreach (var c in results)
            {
                Console.WriteLine($"{ c.CustomerID}: \n");
                foreach (var order in c.orders)
                {
                    Console.WriteLine($"\tOrderID: {order.OrderID}," +
                                      $"\n\tTotal: {order.Total.ToString("C")}," +
                                      $"\n\tDate: {order.OrderDate}\n");
                }
                Console.WriteLine();
            }
        }

        public void Ex15()
        {
            var numC = DataLoader.NumbersC;

            var results = numC.TakeWhile(n => n < 6);

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
        }

        public void Ex16()
        {
            var numC = DataLoader.NumbersC;

            var results = numC.TakeWhile((c, index) => c > index);

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
        }

        public void Ex17()
        {
            var numC = DataLoader.NumbersC;

            var result = numC.SkipWhile(c => c%3 != 0);

            foreach (var i in result)
            {
                Console.WriteLine(i);
            }
        }

        public void Ex18()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                orderby p.ProductName
                select p.ProductName;
            

            foreach (var product in results)
            {
                Console.WriteLine(product);
            }
        }

        public void Ex19()
        {
            var products = DataLoader.LoadProducts();

            var results = products.OrderByDescending(p => p.UnitsInStock);

            foreach (var product in results)
            {
                Console.WriteLine($"{product.ProductName}: {product.UnitsInStock}");
            }
        }

        public void Ex20()
        {
            var products = DataLoader.LoadProducts();

            var results = products.OrderBy(p => p.Category).ThenByDescending(p => p.UnitPrice);

            foreach (var product in results)
            {
                Console.WriteLine($"{product.Category} {product.ProductName}: {product.UnitPrice.ToString("C")}\n");
            }
        }

        public void Ex21()
        {
            var numC = DataLoader.NumbersC;

            var results = numC.Reverse();

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
        }

        public void Ex22()
        {
            var numC = DataLoader.NumbersC;

            var result = numC.OrderBy(c => c%5);

            foreach (var i  in result)
            {
                Console.WriteLine(i.ToString());
            }
        }

        public void Ex23()
        {
            var products = DataLoader.LoadProducts();

            var results = products.GroupBy(p => p.Category);

            foreach (var result in results)
            {
                Console.WriteLine($"{result.Key.ToString()}");
                foreach (var product in result)
                {
                    Console.WriteLine($"\t{product.ProductName}");
                }
                Console.WriteLine();
            }
        }

        public void Ex24()
        {
            //var customers = DataLoader.LoadCustomers();

            //var results = customers.GroupBy(c => c.Orders);

            //foreach (var result in results)
            //{
            //    foreach (var k in result.Key)
            //    {
            //        Console.WriteLine(k.OrderDate.Year);
            //    }
                
            //}

            var customers = DataLoader.LoadCustomers();

            foreach (var customer in customers)
            {
                Console.WriteLine($"{customer.CustomerID}: ");

                foreach (var order in customer.Orders.GroupBy(o=>o.OrderDate.Year).OrderBy(o => o.Key))
                {
                    Console.Write($"\t{order.Key}\n");

                    foreach (var month in customer.Orders.GroupBy(o=>o.OrderDate.Month).OrderBy(o=> o.Key))
                    {
                        Console.Write($"\t{month.Key}");
                    }
                    Console.WriteLine("\n");
                }
            }

           
        }

        public void Ex25()
        {
            var products = DataLoader.LoadProducts();

            var results = products.GroupBy(p => p.Category);

            foreach (var result in results)
            {
                Console.WriteLine($"{result.Key.ToString()}");
            }

        }

        public void Ex26()
        {
            var numB = DataLoader.NumbersB;

            var numA = DataLoader.NumbersA;

            var result = (from a in numA select a)
                .Except(from b in numB select b);
            var result2 = (from a in numB select a)
                .Except(from b in numA select b);

            foreach (var i in result)
            {
                Console.WriteLine(i);
            }
            foreach (var i in result2)
            {
                Console.WriteLine(i);
            }
        }

        public void Ex27()
        {
            var numA = DataLoader.NumbersA;

            var numB = DataLoader.NumbersB;

            var results = numB.Intersect(numA);

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
        }

        public void Ex28()
        {
            var numB = DataLoader.NumbersB;

            var numA = DataLoader.NumbersA;

            var result = (from a in numA select a)
                .Except(from b in numB select b);
            

            foreach (var i in result)
            {
                Console.WriteLine(i);
            }
        }

        public void Ex29()
        {
            var products = DataLoader.LoadProducts();

            var results = products.SkipWhile(p => p.ProductID == 12).Take(1);

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }

        public void Ex30()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Where(p => p.ProductID == 789);

            if (results.Any())
            {
                Console.WriteLine(true);
            }
            else
            {
                Console.WriteLine(false);
            }
           
        }

        public void Ex31()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                where p.UnitsInStock < 1
                select p.Category;

            foreach (var result in results.Distinct())
            {
                Console.WriteLine(result);
            }
        }

        public void Ex32()
        {
            var numB = DataLoader.NumbersB;

            var result = numB.Any(n => n > 9);

            Console.WriteLine(!result);
        }

        public void Ex33()
        {
            var products = DataLoader.LoadProducts();

            //var results = products.GroupBy(p => p.Category);

            //foreach (var result in results)
            //{
            //    if (result.All(r=> r.UnitsInStock > 0))
            //    {
            //        Console.WriteLine(result.Key);

            //    }
            //}

            var results = products.GroupBy(g => g.Category);

            var instock = from r in results
                select new {available = r.All(b => b.UnitsInStock > 0), r.Key};


            foreach (var b in instock)
            {
                if (b.available)
                    Console.WriteLine($"{b.Key}");
            }


        }

        public void Ex34()
        {
            var numsA = DataLoader.NumbersA;

            var results = numsA.Where(c => c%2 != 0);

            Console.WriteLine(results.Count());
        }

        public void Ex35()
        {
            var customers = DataLoader.LoadCustomers();

            var results = from c in customers
                select new {orderCount = c.Orders.Length, c.CustomerID};

            foreach (var result in results)
            {
                Console.WriteLine($"{result.CustomerID}: \n" +
                                  $"\tOrders: {result.orderCount}");
            }
        }

        public void Ex36()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products.GroupBy(c => c.Category)
                select new {productCount = p.Key.Length, p.Key};

            foreach (var result in results)
            {
                Console.WriteLine($"{result.Key} products: {result.productCount}");
            }

        }

        public void Ex37()
        {
                        var products = DataLoader.LoadProducts();

            var results = from p in products.GroupBy(c => c.Category)
                select new {totalStock = +p.Sum(s => s.UnitsInStock), p.Key};

            foreach (var result in results)
            {
                Console.WriteLine($"{result.Key} in stock : {result.totalStock}");
            }

        }

        public void Ex38()
        {
            var products = DataLoader.LoadProducts();

            var results = products.GroupBy(c => c.Category);



            foreach (var result in results)
            {
                Console.WriteLine($"{result.Key} {result.Min(m=> m.UnitPrice).ToString("c")}");
            }

        }

        public void Ex39()
        {
            var products = DataLoader.LoadProducts();

            var results = products.GroupBy(c => c.Category);



            foreach (var result in results)
            {
                Console.WriteLine($"{result.Key} {result.Max(m => m.UnitPrice).ToString("c")}");
            }

        }

        public void Ex40()
        {
            var products = DataLoader.LoadProducts();

            var results = products.GroupBy(c => c.Category);



            foreach (var result in results)
            {
                Console.WriteLine($"{result.Key} {result.Average(m => m.UnitPrice).ToString("c")}");
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.BLL;
using FlooringApplication.Models;
using NUnit.Framework;

namespace FlooringApplication.Tests
{

    [TestFixture]
    public class OrderManagerTests
    {
        [Test]
        public void FoundAccountReturnsSuccess()
        {
            var manager = new OrderManager();
            var response = manager.GetOrder(DateTime.Parse("11/21/2000"), 1);

            Assert.AreEqual("John", response.Data.CustomerName);
            Assert.AreEqual("Wood", response.Data.Product.Type);
        }

        [Test]
        public void NotFoundAccountReturnsFail()
        {
            var manager = new OrderManager();
            var response = manager.CheckDate(new DateTime(10/21/2000));
            Assert.IsFalse(response.Success);
        }

        [Test]
        public void AddOrderSuccessfull()
        {
            var manager = new OrderManager();
            var order = manager.CalculateOrder(new Order()
            {
                DateTime = DateTime.Parse("11/20/2000"),
                CustomerName = "Jerry",
                Area = 55,
                Product = new Product() { CostPerSf = 4, LaborCostPerSf = 5, Type = "Tile"},
                State = new State() { StateName = "Kansas", TaxRate = 9.40m, Abbreviation = "KS"},
            });
            manager.CreateDate(order.DateTime);
            manager.AddOrder(order);

            var orders = manager.GetAllOrders(order.DateTime).Count;
            Assert.AreEqual(1,orders);
        }

        [Test]
        public void RemoveOrderSuccessfull()
        {
            var manager = new OrderManager();
            var order = new Order()
            {
                OrderNumber = 1,
                CustomerName = "John",
                Area = 21,
                DateTime = DateTime.Parse("11/21/2000"),
                Product = new Product() {CostPerSf = 20, LaborCostPerSf = 15, Type = "Wood"},
                State = new State() {StateName = "Kansas", Abbreviation = "KS", TaxRate = 0.09m},
                LaborCost = 315,
                MaterialCost = 420,
                TaxCost = 66.15m,
                Total = 801.15m
            };
            manager.RemoveOrder(order);
            var orderLength = manager.GetAllOrders(order.DateTime).Count;

            Assert.AreEqual(1,orderLength);
        }
    }

}

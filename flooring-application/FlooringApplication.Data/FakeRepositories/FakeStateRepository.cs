﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.Data.Repo_Interfaces;
using FlooringApplication.Models;

namespace FlooringApplication.Data
{
    public class FakeStateRepository : IStateRepository
    {
        private static List<State> AllStates;

        public FakeStateRepository()
        {
            AllStates = new List<State>();

            if (AllStates.Count == 0)
            {
                AllStates = new List<State>()
                {
                    new State() {StateName = "Kansas", Abbreviation = "KS", TaxRate = .09m},
                    new State() {StateName = "Ohio", Abbreviation = "OH", TaxRate = .08m},
                };
            }
        }

        public List<State> GetAllStates()
        {
            return AllStates;
        }
    }
}

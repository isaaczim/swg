﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.Models;

namespace FlooringApplication.Data
{
    public interface IOrderRepository
    {
        List<Order> GetAllOrderPerDate(DateTime date);
        Order GetOrder(int orderNumber, List<Order> orders );
        void UpdateDate(Order order);
        void AddOrder(Order order);
        Response<List<Order>> CheckDate(DateTime date);
        Response<DateTime> CreateDate(DateTime date);
        void RemoveOrder(Order order);
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.Data.Repo_Interfaces;
using FlooringApplication.Models;

namespace FlooringApplication.Data
{
    public class StateRepository : IStateRepository
    {
        private static List<State> AllStates;
        private const string _FilePath = @"DataFiles/Taxes.txt";

        public StateRepository()
        {
            GetAllStates();
        }

        public List<State> GetAllStates()
        {
            List<State> results = new List<State>();
            var rows = File.ReadAllLines(_FilePath);

            for (int i = 1; i < rows.Length; i++)
            {
                var columns = rows[i].Split(',');
                var state = new State();
                state.Abbreviation = columns[0];
                state.StateName = columns[1];
                state.TaxRate = decimal.Parse(columns[2]);
                results.Add(state);
            }
            return results;
        }
    }
}

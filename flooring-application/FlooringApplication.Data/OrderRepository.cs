﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FlooringApplication.Models;

namespace FlooringApplication.Data
{
    public class OrderRepository : IOrderRepository
    {
        private string _filePath = @"DataFiles/";

        public List<Order> GetAllOrderPerDate(DateTime date)
        {
            _filePath = $"DataFiles/Orders_{date.Month}{date.Day}{date.Year}.txt";
            List<Order> results = new List<Order>();
            var rows = File.ReadAllLines(_filePath);

            for (int i = 1; i < rows.Length; i++)
            {
                var columns = rows[i].Split(',');

                var order = new Order();
                order.CustomerName = columns[0];
                order.DateTime = DateTime.Parse(columns[1]);
                order.Area = decimal.Parse(columns[2]);
                order.Product = new Product();
                order.Product.Type = columns[3];
                order.Product.CostPerSf = decimal.Parse(columns[4]);
                order.Product.LaborCostPerSf = decimal.Parse(columns[5]);
                order.State = new State();
                order.State.StateName = columns[6];
                order.State.Abbreviation = columns[7];
                order.State.TaxRate = decimal.Parse(columns[8]);
                order.LaborCost = decimal.Parse(columns[9]);
                order.MaterialCost = decimal.Parse(columns[10]);
                order.OrderNumber = int.Parse(columns[11]);
                order.TaxCost = decimal.Parse(columns[12]);
                order.Total = decimal.Parse(columns[13]);
                

                results.Add(order);
            }

            return results;
        }

        public Order GetOrder(int orderNumber, List<Order> orders)
        {
            var result = orders.FirstOrDefault(o => o.OrderNumber == orderNumber);
            var test = result;
            return test;
        }

        public void AddOrder(Order order)
        {
            _filePath = $"DataFiles/Orders_{order.DateTime.Month}{order.DateTime.Day}{order.DateTime.Year}.txt";
            var orders = GetAllOrderPerDate(order.DateTime);
            if (orders.Any())
            {
                int i = orders[orders.Count - 1].OrderNumber;
                order.OrderNumber = i + 1;
            }
            else
            {
                order.OrderNumber = 1;
            }
            orders.Add(order);
            using (var writer = File.AppendText(_filePath))
            {
                writer.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13}",
                        order.CustomerName,
                        order.DateTime,
                        order.Area,
                        order.Product.Type,
                        order.Product.CostPerSf,
                        order.Product.LaborCostPerSf,
                        order.State.StateName,
                        order.State.Abbreviation,
                        order.State.TaxRate,
                        order.LaborCost,
                        order.MaterialCost,
                        order.OrderNumber,
                        order.TaxCost,
                        order.Total);
            }
        }

        private void WriteFile(List<Order> orders, DateTime date)
        {
            _filePath = $"DataFiles/Orders_{date.Month}{date.Day}{date.Year}.txt";

            using (var writer = File.CreateText(_filePath))
            {
                writer.WriteLine("CustomerName,DateTime,Area,ProductType,CostPerSf,LaborCostPerSf,StateName,Abbreviation,TaxRate,LaborCost,MaterialCost,OrderNumber,TaxCost,Total");
                foreach (var order in orders)
                {
                    writer.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13}",
                        order.CustomerName,
                        order.DateTime,
                        order.Area,
                        order.Product.Type,
                        order.Product.CostPerSf,
                        order.Product.LaborCostPerSf,
                        order.State.StateName,
                        order.State.Abbreviation,
                        order.State.TaxRate,
                        order.LaborCost,
                        order.MaterialCost,
                        order.OrderNumber,
                        order.TaxCost,
                        order.Total);
                }
            }
        }

        public void UpdateDate(Order order)
        {
            var orders = GetAllOrderPerDate(order.DateTime);
            orders.Add(order);
            WriteFile(orders, order.DateTime);
        }

        public Response<List<Order>> CheckDate(DateTime date)
        {
            Response<List<Order>> response = new Response<List<Order>>();
            if (File.Exists($"DataFiles/Orders_{date.Month}{date.Day}{date.Year}.txt"))
            {
                response.Success = true;
                response.Data = GetAllOrderPerDate(date);
            }
            else
            {
                response.Success = false;
                response.Message = "Could not find date";
            }
            return response;
        }

        public Response<DateTime> CreateDate(DateTime date)
        {
            _filePath = $"DataFiles/Orders_{date.Month}{date.Day}{date.Year}.txt";
            using (var writer = File.CreateText(_filePath))
            {
                writer.WriteLine("CustomerName,DateTime,Area,ProductType,CostPerSf,LaborCostPerSf,StateName,Abbreviation,TaxRate,LaborCost,MaterialCost,OrderNumber,TaxCost,Total");
            }
            Response<DateTime> response = new Response<DateTime>();
            return response;
        }

        public void RemoveOrder(Order order)
        {
            _filePath = $"DataFiles/Orders_{order.DateTime.Month}{order.DateTime.Day}{order.DateTime.Year}.txt";
            var orders = GetAllOrderPerDate(order.DateTime);
            var results = orders.Where(o => o.OrderNumber != order.OrderNumber);
            if (!results.Any())
            {
                File.Delete(_filePath);
            }
            else
            {
                WriteFile(results.ToList(), order.DateTime);
            }
        }
    }
}

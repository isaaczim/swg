﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringApplication.Data.Factories
{
    public class OrderFactory
    {
        public static IOrderRepository GetOrderRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];

            switch (mode)
            {
                case "Test":
                    return new FakeOrderRepository();
                default:
                    return new OrderRepository();
            }
        }
    }
}

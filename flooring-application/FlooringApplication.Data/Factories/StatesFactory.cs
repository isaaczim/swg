﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.Data.Repo_Interfaces;

namespace FlooringApplication.Data.Factories
{
    public class StatesFactory
    {
        public static IStateRepository GetStateRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];

            switch (mode)
            {
                case "Test":
                    return new FakeStateRepository();
                default:
                    return new StateRepository();
            }
        }
    }
}

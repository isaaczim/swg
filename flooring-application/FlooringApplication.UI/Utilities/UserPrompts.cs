﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.BLL;
using FlooringApplication.Models;

namespace FlooringApplication.UI.Utilities
{
    public static class UserPrompts
    {
        public static Response<Product> GetProductFromUser(string message)
        {
            ProductManager manager = new ProductManager();
            Console.Write(message);
            string input = Console.ReadLine();
            Response<Product> response = new Response<Product>();
            response = manager.GetProduct(input);
            return response;
        }

        public static char GetAnswerFromUser(string message)
        {
            Console.Write(message);
            while (true)
            {
                string input = Console.ReadLine();
                if (input.Substring(0,1).ToUpper() == "Y")
                    return 'Y';
                
                else if (input.Substring(0,1).ToUpper() == "N")
                    return 'N';
                
                else
                {
                    Console.Write("Please enter a valid answer(Y/N): ");
                }
            }

        }

        public static Response<State> GetStateFromUser(string message)
        {
            StateManager manager = new StateManager();
            Console.Write(message);
            string input = Console.ReadLine();
            Response<State> response = new Response<State>();
            response = manager.GetState(input);
            return response;
        }

        public static Response<DateTime> GetDateFromUser(string message)
        {
            OrderManager manager = new OrderManager();
            Console.Write($"{message} (DD/MM/YYYY): ");
            string input = Console.ReadLine();
            DateTime date;
            Response<DateTime> response = new Response<DateTime>();
            if (DateTime.TryParse(input, out date))
            {
                var dateResponse = manager.CheckDate(date);
                if (!dateResponse.Success)
                {
                    response.Success = false;
                    response.Data = date;
                    response.Message = "Date does not exist.";
                    return response;
                }
                response.Success = true;
                response.Data = date;

            }
            else
            {
                response.Success = false;
                response.Message = "Unable to convert date.";
            }
            return response;
        }


        public static int GetIntFromUser(string message)
        {

            do
            {
                
                Console.Write(message);
                string input = Console.ReadLine();
                int value;
                if (int.TryParse(input, out value))
                    return value;

                Console.WriteLine("That was not a valid number.");
                PressKeyForContinue();

            } while (true);
        }

        public static Response<decimal> GetDecimalFromUser(string message)
        {
            Response<decimal> response = new Response<decimal>();
                Console.Write(message);
                var input = Console.ReadLine();
                decimal value;

            if (decimal.TryParse(input, out value) && value > 0)
            {
                response.Data = value;
                response.Success = true;
            }
            else
            {
                response.Message = ("That was not a valid value.");
                response.Success = false;
            }
            return response;

        }

        public static void PressKeyForContinue()
        {
            Console.Write("Press any key to continue...");
            Console.ReadKey();
        }

        public static string GetStringFromUser(string message)
        {
            Console.Write(message);
            return Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.Models;

namespace FlooringApplication.UI.Utilities
{
    public class Screens
    {
        public static void DisplayOrder(Order order)
        {
            if(order.CustomerName != null)
            Console.WriteLine($"***********************************************\n" +
                              $"*\n" +
                              $"* Name on Order: {order.CustomerName}\n" +
                              $"* Order #: {order.OrderNumber}\n" +
                              $"*\n" +
                              $"* Product info: \n" +
                              $"* \tType: {order.Product.Type}\n" +
                              $"* \tCost Per Square Foot: {order.Product.CostPerSf:C}\n" +
                              $"* \tLabor Cost Per Squar Foot: {order.Product.LaborCostPerSf:c}\n" +
                              $"* \tArea of Order: { order.Area}\n" +
                              $"*\n" +
                              $"* State: {order.State.StateName}\n" +
                              $"* \tTax Rate: {(order.State.TaxRate)}%\n" +
                              $"*\n" +
                              $"* Cost:\n" +
                              $"* \tMaterial Cost: {order.MaterialCost:c}\n" +
                              $"* \tLabor Cost: {order.LaborCost:c}\n" +
                              $"* \tTax: {order.TaxCost:c}\n" +
                              $"* \tTotal: {order.Total:C}\n" +
                              $"*\n" +
                              $"***********************************************");
        }
    }
}

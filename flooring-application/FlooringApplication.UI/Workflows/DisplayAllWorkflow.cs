﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.BLL;
using FlooringApplication.Models;
using FlooringApplication.UI.Utilities;

namespace FlooringApplication.UI.Workflows
{
    public class DisplayAllWorkflow : IWorkflow
    {
        public void Execute()
        {
            OrderManager manager = new OrderManager();
            Console.Clear();
            Response<DateTime> response = UserPrompts.GetDateFromUser("Please enter the date of the orders you would like to view");
            if (response.Success)
            {
                var orders = manager.GetAllOrders(response.Data);
                foreach (var order in orders)
                {
                    Screens.DisplayOrder(order);
                }
                UserPrompts.PressKeyForContinue();
            }
            else
            {
                Console.WriteLine(response.Message);
                UserPrompts.PressKeyForContinue();
            }
        }
    }
}

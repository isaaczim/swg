﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.UI.Utilities;

namespace FlooringApplication.UI.Workflows
{
    public class MainMenu
    {
        public void Exacute()
        {
            do
            {
                Console.Clear();
                Console.WriteLine
(@"*************************************************
*
*
* 1. Display Orders
*
* 2. Add Order
*
* 3. Edit Order
*
* 4. Delete Order
*
* 5. Quit
*
*
**************************************************");
                string input = UserPrompts.GetStringFromUser("\nEnter Choice: ");

                ProcessChoice(input);

            } while (true);
        }



        private void ProcessChoice(string choice)
        {
            IWorkflow _workflow;
            switch (choice)
            {
                case "1":
                    _workflow = new DisplayAllWorkflow();
                    _workflow.Execute();
                    break;
                case "2":
                    _workflow = new AddOrderWorkflow();
                    _workflow.Execute();
                    break;
                case "3":
                    _workflow = new EditOrderWorkflow();
                    _workflow.Execute();
                    break;
                case "4":
                    _workflow = new RemoveWorkflow();
                    _workflow.Execute();
                    break;
                case "5":
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Please use a valid input.");
                    break;
            }
        }
    }
}


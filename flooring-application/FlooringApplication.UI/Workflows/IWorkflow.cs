﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.BLL;

namespace FlooringApplication.UI.Workflows
{
    public interface IWorkflow
    {
        void Execute();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.BLL;
using FlooringApplication.UI.Utilities;

namespace FlooringApplication.UI.Workflows
{
    public class RemoveWorkflow : IWorkflow
    {
        public void Execute()
        {
            OrderManager manager = new OrderManager();
            Console.Clear();
            var dateResponse = UserPrompts.GetDateFromUser("Please enter the date of the order you would like to remove: ");
            if (dateResponse.Success)
            {
                Console.Clear();
                var orders = manager.GetAllOrders(dateResponse.Data);
                foreach (var order in orders)
                {
                    Screens.DisplayOrder(order);
                    Console.WriteLine();
                }
                int n = UserPrompts.GetIntFromUser("Please enter the order number of the order you would like to remove: ");
                var OrderToRemove = manager.GetOrder(manager.GetAllOrders(dateResponse.Data), n);
                if (OrderToRemove.Success)
                {
                    Console.Clear();
                    Screens.DisplayOrder(OrderToRemove.Data);
                    char confirm =
                        UserPrompts.GetAnswerFromUser(
                            "Are you sure you would like to Delete this Order? \n" +
                            "It cannot be undone. (Y/N): ");
                    if (confirm == 'Y')
                    {
                        manager.RemoveOrder(OrderToRemove.Data);
                    }
                }
            }
            else
            {
                Console.WriteLine(dateResponse.Message);
                UserPrompts.PressKeyForContinue();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace FlooringApplication.Models
{
    public class Product
    {
        public string Type { get; set; }
        public decimal CostPerSf { get; set; }
        public decimal LaborCostPerSf { get; set; }
    }
}

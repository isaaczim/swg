﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringApplication.Models
{
    public class Order
    {
        public Product Product { get; set; }
        public State State { get; set; }
        public DateTime DateTime { get; set; }
        public decimal Area { get; set; }
        public int OrderNumber { get; set; }
        public decimal MaterialCost { get; set; }
        public decimal TaxCost { get; set; }
        public string CustomerName { get; set; }
        public decimal LaborCost { get; set; }
        public decimal Total { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.Data;
using FlooringApplication.Data.Factories;
using FlooringApplication.Data.Repo_Interfaces;
using FlooringApplication.Models;

namespace FlooringApplication.BLL
{
    public class StateManager
    {
        private IStateRepository _repo;

        public StateManager()
        {
            _repo = StatesFactory.GetStateRepository();
        }

        public Response<State> GetState(string input)
        {
            Response<State> response = new Response<State>();
            State newState = _repo.GetAllStates().FirstOrDefault(p => p.Abbreviation.ToUpper() == (string)input.ToUpper());
            if (newState == null)
            {
                response.Message = "No State with this abbreviation.";
                response.Success = false;
            }
            else
            {
                response.Data = newState;
                response.Success = true;
            }
            return response;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.Data;
using FlooringApplication.Data.Factories;
using FlooringApplication.Data.Repo_Interfaces;
using FlooringApplication.Models;

namespace FlooringApplication.BLL
{
    public class ProductManager
    {
        private IProductRepository _repo;

        public ProductManager()
        {
            _repo = ProductFactory.GetProductRepository();
        }

        public Response<Product> GetProduct(string input)
        {
            Response<Product> response = new Response<Product>();
            Product newProduct = _repo.GetAllProducts().FirstOrDefault(p => p.Type.ToUpper() == (string)input.ToUpper());
            if (newProduct  == null)
            {
                response.Message = "No Product with this type.";
                response.Success = false;
            }
            else
            {
                response.Data = newProduct;
                response.Success = true;
            }
            return response;
        }
    }
}

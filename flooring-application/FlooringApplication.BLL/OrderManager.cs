﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.Data;
using FlooringApplication.Data.Factories;
using FlooringApplication.Models;

namespace FlooringApplication.BLL
{
    public class OrderManager
    {
        private IOrderRepository _repo;

        public OrderManager()
        {
            _repo = OrderFactory.GetOrderRepository();
        }

        public void AddOrder(Order order)
        {
            _repo.AddOrder(order);
        }

        public Order CalculateOrder(Order orderToCalculate)
        {
            Order newOrder = new Order();
            newOrder = orderToCalculate;
            newOrder.LaborCost = newOrder.Product.LaborCostPerSf*newOrder.Area;
            newOrder.MaterialCost = newOrder.Product.CostPerSf*newOrder.Area;
            newOrder.TaxCost = ((newOrder.LaborCost + newOrder.MaterialCost)*newOrder.State.TaxRate)/100;
            newOrder.Total = newOrder.TaxCost + newOrder.LaborCost + newOrder.MaterialCost;
            return newOrder;
        }

        public List<Order> GetAllOrders(DateTime date)
        {
            return _repo.GetAllOrderPerDate(date);
        }

        public Response<Order> GetOrder(List<Order> orders, int OrderNumber)
        {
            var order = _repo.GetOrder(OrderNumber, orders);
            Response<Order> response = new Response<Order>();
            if (order != null)
            {
                response.Data = order;
                response.Success = true;
            }
            else
            {
                response.Message = "No order with this number";
                response.Success = false;
            }
            return response;
        }

        public Response<Order> GetOrder(DateTime date, int OrderNumber)
        {
            Response<Order> orderResponse = new Response<Order>();
            if (CheckDate(date).Success)
            {
                var orders = GetAllOrders(date);
                orderResponse = GetOrder(orders, OrderNumber);
                orderResponse.Success = true;
            }
            else
            {
                orderResponse.Success = false;
            }
            return orderResponse;
        }

        public void RemoveOrder(Order order)
        {
            _repo.RemoveOrder(order);
        }

        public Response<List<Order>> CheckDate(DateTime date)
        {
            var response = _repo.CheckDate(date);
            if (response.Success)
            {
                return response;
            }
            return response;
        }

        public void CreateDate(DateTime date)
        {
            _repo.CreateDate(date);
        }


    }
}

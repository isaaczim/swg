﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithLinq
{
    class Program
    {
        static void Main(string[] args)
        {
            ProductInfo[] itemsinStock = new[]
            {
                new ProductInfo()
                {
                    Name = "Mac's Coffe",
                    Description = "Coffe with Teeth",
                    NumberInStock = 24
                },
                new ProductInfo()
                {
                    Name = "John's Coffe",
                    Description = "Coffe without Teeth",
                    NumberInStock = 100
                },
                new ProductInfo()
                {
                    Name = "Lue's Coffe",
                    Description = "Coffe with Wings",
                    NumberInStock = 120
                },
                new ProductInfo()
                {
                    Name = "Ron's Coffe",
                    Description = "Coffe Black as your Soul",
                    NumberInStock = 2
                },
                new ProductInfo()
                {
                    Name = "Sam's Coffe",
                    Description = "Coffe Sweet as You",
                    NumberInStock = 100
                },
                new ProductInfo()
                {
                    Name = "Mom's Coffe",
                    Description = "Original",
                    NumberInStock = 73
                },
            };

            SelectEverything(itemsinStock);
            Console.WriteLine();
            ListProductNames(itemsinStock);
            Console.WriteLine();
            GetOverStockProduct(itemsinStock);
            Console.WriteLine();
            GetNamesAndDescription(itemsinStock);
            Console.WriteLine();
            GetCount();
            Console.WriteLine();
            AlphabetizeNames(itemsinStock);
            Console.WriteLine();



            Console.ReadLine();
        }

        static void SelectEverything(ProductInfo[] product)
        {
            Console.WriteLine("all product detail : ");

            var allProducts = from p in product
                select p;

            foreach (var productInfo in allProducts)
            {
                Console.WriteLine( productInfo.ToString());
            }
        }

        static void ListProductNames(ProductInfo[] products)
        {
            Console.WriteLine("Only Product info: ");
            var names = from p in products
                select p.Name;

            foreach (var name in names)
            {
                Console.WriteLine("Name: {0}", name);
            }
        }

        static void GetOverStockProduct(ProductInfo[] products)
        {
            Console.WriteLine("Overstock Items: ");

            var overstock = from p in products
                where p.NumberInStock > 25
                select p;

            foreach (var productInfo in overstock)
            {
                Console.WriteLine(productInfo.ToString());
            }
        }

        static void GetNamesAndDescription(ProductInfo[] products)
        {
            Console.WriteLine("Names and Description");

            var nameDesc = from p in products
                where p.NumberInStock >= 100
                select new {p.Name, p.Description};

            foreach (var item in nameDesc)
            {
                Console.WriteLine("Name: {0}, Description: {1}", item.Name, item.Description);
            }
        }

        static void GetCount()
        {
            string[] videoGames = {"Fallout 4", "Halo", "Disney Infity", "Project Spark", "COD"};

            int number = (from g in videoGames
                where g.Length > 6
                select g).Count();

            Console.WriteLine("{0} items in query", number);
        }

        static void AlphabetizeNames(ProductInfo[] products)
        {
            var subset = from p in products
                orderby p.Name
                select p;

            Console.WriteLine("Product in order");
            foreach (var productInfo in subset)
            {
                Console.WriteLine(productInfo.ToString());
            }
        }
    }
}

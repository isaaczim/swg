﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkSamples
{
    class Program
    {
        static void Main(string[] args)
        {
            //ShowAnonymousTypes();
            //ShowLinqJoins();
            //showLinqGrouping();


            Console.ReadLine();
        }

        static void showLinqGrouping()
        {
            List<Student> students = new List<Student>()
            {
                new Student() {LastName = "sandler", Major = "Computer Science"},
                new Student() {LastName = "martin", Major = "History"},
                new Student() {LastName = "doe", Major = "English"},
                new Student() {LastName = "Pacino", Major = "Computer Science"},
                new Student() {LastName = "john", Major = "History"},

            };

            var result = from student in students
                          group student by
            student.Major;

            foreach (var group in result)
            {
                Console.WriteLine(group.Key);
                foreach (var student in group)
                {
                    Console.WriteLine("\t{0}", student.LastName);
                }
            }
        }
        static void ShowLinqJoins()
        {
            List<Student> students = new List<Student>()
            {
                new Student() { LastName = "wise", StudentID = 1},
                new Student() { LastName = "ward", StudentID = 2}
            };
            
            List<StudentCourse> courses = new List<StudentCourse>()
            {
                new StudentCourse() {StudentID = 1, StudentCoarse = "C# fundamentals"},
                new StudentCourse() {StudentID = 1, StudentCoarse = "Asp.net Fundamentals"},
                new StudentCourse() {StudentID = 2, StudentCoarse = "Java Fund"},
                new StudentCourse() {StudentID = 2, StudentCoarse = "CSS and HTML"}
            };

            var results = from student in students
                join course in courses
                    on student.StudentID equals course.StudentID
                select new {studentName = student.LastName, course.StudentCoarse};
                //select new {course, student};

            Console.WriteLine("student coarse;");
            foreach (var result in results)
            {
                Console.WriteLine("{0, -10} {1}", result.studentName, result.StudentCoarse);
            }
        }

        static void ShowAnonymousTypes()
        {
            Console.WriteLine("**** Show Anonymous ****");
            List<Person> people = makePeople();

            var ladies = from p in people
                where p.Gender == 'f'
                select new {Name = p.FirstName + ", " + p.LastName, p.Age};
                //select p;

            foreach (var lady in ladies)
            {
                Console.WriteLine($"{lady.Name}, age = {lady.Age}");
            }
        }

        static List<Person> makePeople()
        {
            return new List<Person>()
            {
                new Person {FirstName = "homer", LastName = "simpson", Age = 40, Gender = 'm'},
                new Person {FirstName = "marge", LastName = "simpson", Age = 38, Gender = 'f'},
                new Person {FirstName = "bart", LastName = "simpson", Age = 11, Gender = 'm'},
                new Person {FirstName = "lisa", LastName = "simpson", Age = 9, Gender = 'f'},
            };
        }
    }
}

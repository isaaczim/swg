﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkSamples
{
    public class Student
    {
        public int StudentID { get; set; }
        public string LastName { get; set; }
        public string Major { get; set; }
    }
}

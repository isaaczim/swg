USE [master]
GO
/****** Object:  Database [DvdCollection]    Script Date: 6/19/2016 12:14:02 PM ******/
CREATE DATABASE [DvdCollection]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DvdCollection', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\DvdCollection.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DvdCollection_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\DvdCollection_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DvdCollection] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DvdCollection].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DvdCollection] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DvdCollection] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DvdCollection] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DvdCollection] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DvdCollection] SET ARITHABORT OFF 
GO
ALTER DATABASE [DvdCollection] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DvdCollection] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DvdCollection] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DvdCollection] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DvdCollection] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DvdCollection] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DvdCollection] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DvdCollection] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DvdCollection] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DvdCollection] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DvdCollection] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DvdCollection] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DvdCollection] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DvdCollection] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DvdCollection] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DvdCollection] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DvdCollection] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DvdCollection] SET RECOVERY FULL 
GO
ALTER DATABASE [DvdCollection] SET  MULTI_USER 
GO
ALTER DATABASE [DvdCollection] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DvdCollection] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DvdCollection] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DvdCollection] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [DvdCollection] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'DvdCollection', N'ON'
GO
USE [DvdCollection]
GO
/****** Object:  Table [dbo].[ActorJunction]    Script Date: 6/19/2016 12:14:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActorJunction](
	[ActorID] [int] NOT NULL,
	[DvdID] [int] NOT NULL,
 CONSTRAINT [PK_ActorJunction] PRIMARY KEY CLUSTERED 
(
	[ActorID] ASC,
	[DvdID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Actors]    Script Date: 6/19/2016 12:14:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Actors](
	[ActorID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_Actors] PRIMARY KEY CLUSTERED 
(
	[ActorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Borrowers]    Script Date: 6/19/2016 12:14:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Borrowers](
	[BorrowerID] [int] NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_Borrowers] PRIMARY KEY CLUSTERED 
(
	[BorrowerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DirectorJunction]    Script Date: 6/19/2016 12:14:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DirectorJunction](
	[DirectorID] [int] NOT NULL,
	[DvdID] [int] NOT NULL,
 CONSTRAINT [PK_DirectorJunction] PRIMARY KEY CLUSTERED 
(
	[DirectorID] ASC,
	[DvdID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Directors]    Script Date: 6/19/2016 12:14:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Directors](
	[DirectorID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_Directors] PRIMARY KEY CLUSTERED 
(
	[DirectorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Dvds]    Script Date: 6/19/2016 12:14:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dvds](
	[DvdID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nchar](30) NOT NULL,
	[ReleaseDate] [datetime] NOT NULL,
	[Details] [varchar](2000) NULL,
	[ImageLink] [varchar](200) NULL,
	[RatingID] [int] NULL,
	[BorrowerID] [int] NULL,
 CONSTRAINT [PK_Dvds] PRIMARY KEY CLUSTERED 
(
	[DvdID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoanHistory]    Script Date: 6/19/2016 12:14:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoanHistory](
	[HistoryID] [int] NOT NULL,
	[DateBorrowed] [datetime] NOT NULL,
	[DateReturned] [datetime] NULL,
	[FK_BorrowerID] [int] NULL,
 CONSTRAINT [PK_LoanHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ratings]    Script Date: 6/19/2016 12:14:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ratings](
	[RatingID] [int] IDENTITY(1,1) NOT NULL,
	[Rating] [nchar](5) NOT NULL,
 CONSTRAINT [PK_Ratings] PRIMARY KEY CLUSTERED 
(
	[RatingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StudioJunction]    Script Date: 6/19/2016 12:14:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudioJunction](
	[StudioID] [int] NOT NULL,
	[DvdID] [int] NOT NULL,
 CONSTRAINT [PK_StudioJunction] PRIMARY KEY CLUSTERED 
(
	[StudioID] ASC,
	[DvdID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Studios]    Script Date: 6/19/2016 12:14:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Studios](
	[StudioID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_Studios] PRIMARY KEY CLUSTERED 
(
	[StudioID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ActorJunction]  WITH CHECK ADD  CONSTRAINT [FK_ActorJunction_Actors] FOREIGN KEY([ActorID])
REFERENCES [dbo].[Actors] ([ActorID])
GO
ALTER TABLE [dbo].[ActorJunction] CHECK CONSTRAINT [FK_ActorJunction_Actors]
GO
ALTER TABLE [dbo].[ActorJunction]  WITH CHECK ADD  CONSTRAINT [FK_ActorJunction_Dvds] FOREIGN KEY([DvdID])
REFERENCES [dbo].[Dvds] ([DvdID])
GO
ALTER TABLE [dbo].[ActorJunction] CHECK CONSTRAINT [FK_ActorJunction_Dvds]
GO
ALTER TABLE [dbo].[DirectorJunction]  WITH CHECK ADD  CONSTRAINT [FK_DirectorJunction_Directors] FOREIGN KEY([DirectorID])
REFERENCES [dbo].[Directors] ([DirectorID])
GO
ALTER TABLE [dbo].[DirectorJunction] CHECK CONSTRAINT [FK_DirectorJunction_Directors]
GO
ALTER TABLE [dbo].[DirectorJunction]  WITH CHECK ADD  CONSTRAINT [FK_DirectorJunction_Dvds] FOREIGN KEY([DvdID])
REFERENCES [dbo].[Dvds] ([DvdID])
GO
ALTER TABLE [dbo].[DirectorJunction] CHECK CONSTRAINT [FK_DirectorJunction_Dvds]
GO
ALTER TABLE [dbo].[Dvds]  WITH CHECK ADD  CONSTRAINT [FK_Dvds_Borrowers] FOREIGN KEY([BorrowerID])
REFERENCES [dbo].[Borrowers] ([BorrowerID])
GO
ALTER TABLE [dbo].[Dvds] CHECK CONSTRAINT [FK_Dvds_Borrowers]
GO
ALTER TABLE [dbo].[Dvds]  WITH CHECK ADD  CONSTRAINT [FK_Dvds_Ratings] FOREIGN KEY([RatingID])
REFERENCES [dbo].[Ratings] ([RatingID])
GO
ALTER TABLE [dbo].[Dvds] CHECK CONSTRAINT [FK_Dvds_Ratings]
GO
ALTER TABLE [dbo].[LoanHistory]  WITH CHECK ADD  CONSTRAINT [FK_LoanHistory_Borrowers] FOREIGN KEY([FK_BorrowerID])
REFERENCES [dbo].[Borrowers] ([BorrowerID])
GO
ALTER TABLE [dbo].[LoanHistory] CHECK CONSTRAINT [FK_LoanHistory_Borrowers]
GO
ALTER TABLE [dbo].[StudioJunction]  WITH CHECK ADD  CONSTRAINT [FK_StudioJunction_Dvds] FOREIGN KEY([DvdID])
REFERENCES [dbo].[Dvds] ([DvdID])
GO
ALTER TABLE [dbo].[StudioJunction] CHECK CONSTRAINT [FK_StudioJunction_Dvds]
GO
ALTER TABLE [dbo].[StudioJunction]  WITH CHECK ADD  CONSTRAINT [FK_StudioJunction_Studios] FOREIGN KEY([StudioID])
REFERENCES [dbo].[Studios] ([StudioID])
GO
ALTER TABLE [dbo].[StudioJunction] CHECK CONSTRAINT [FK_StudioJunction_Studios]
GO
/****** Object:  StoredProcedure [dbo].[AddDirector]    Script Date: 6/19/2016 12:14:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[AddDirector](
 @DirectorName varchar(50)
 )as
 
 insert Directors(Name)
 values (@DirectorName)
GO
/****** Object:  StoredProcedure [dbo].[AddDvd]    Script Date: 6/19/2016 12:14:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[AddDvd]
(
	@Title nchar(30),
	@ReleaseDate datetime,
	@ImageLink varchar(200) null,
	@Details nchar(50) null,
	@RatingID int null
)AS
	Insert Dvds(Title, ReleaseDate, ImageLink, Details, RatingID)
	values(@Title, @ReleaseDate, @ImageLink, @Details, @RatingID)
GO
USE [master]
GO
ALTER DATABASE [DvdCollection] SET  READ_WRITE 
GO

﻿$(document)
    .ready(function () {
        $('#AddDirector')
            .validate({
                rules: {
                    "Name": {
                        required: true
                    }
                },
                messages: {
                    "Name": "Please enter a director name"
                }

            });
    });
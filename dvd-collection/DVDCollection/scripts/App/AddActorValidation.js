﻿$(document)
    .ready(function () {
        $('#AddNewActor')
            .validate({
                rules: {
                    "Actor.Name": {
                        required: true
                    }
                },
                messages: {
                    "Actor.Name": "Please enter an actor name"
                }

            });
    });
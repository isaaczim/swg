﻿$(document)
    .ready(function () {
        $('#AddStudio')
            .validate({
                rules: {
                    "Name": {
                        required: true
                    }
                },
                messages: {
                    "Name": "Please enter a studio name"
                }

            });
    });
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DataLayer.Data
{
    public class Director
    {
       
        public string Name { get; set; }
        public int DirectorID { get; set; }
    }
}
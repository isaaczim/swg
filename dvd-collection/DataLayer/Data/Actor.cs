﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DataLayer.Data
{
    public class Actor
    {
        
        public string Name { get; set; }
        public int ActorID { get; set; }
    }
}
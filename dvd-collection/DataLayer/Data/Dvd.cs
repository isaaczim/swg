﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Xml.Schema;

namespace DataLayer.Data
{
    public class Dvd
    {
        public int DvdID { get; set; }

        //[Required(ErrorMessage = "Enter a Title.")]
        public string Title { get; set; }


        [DisplayName("ReleaseDate"),DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }

        public List<Actor> Actors { get; set; }

        //[Required(ErrorMessage = "Enter a Description.")]
        public string Details { get; set; }

        //[Required(ErrorMessage = "Enter a link to an Image.")]
        public string ImageLink { get; set; }
        public int? RatingID { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Data;

namespace DataLayer.ViewModel
{
    public class DvdVM
    {
        public Dvd Dvd { get; set; }
        public List<Actor> ActorsList { get; set; }
        public Director Director { get; set; }
        public Studio Studio { get; set; }
        public Actor Actor { get; set; }


    }
}

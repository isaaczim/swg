﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DataLayer.Config;
using DataLayer.Data;

namespace DataLayer.Repositories
{
    public class StudioRepositroy
    {
        public List<Studio> GetAllStudios()
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var studios = cn.Query<Studio>("Select * from studios");
                return studios.ToList();
            }
        }

        public void AddStudio(Studio studio)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                cn.Execute("Insert studios(name) values(@Name)", new { @Name = studio.Name });
            }
        }

        public void AddDvd(int studioId, int dvdId)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("dvdId", dvdId);
                p.Add("StudioId", studioId);
                cn.Execute("Insert studiojunction(dvdid, studioid) values(@dvdId, @StudioId)",
                    p);
            }
        }

        public Studio GetStudio(int id)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var studio = cn.Query<Studio>(
                    "Select name from studios d inner join studiojunction dj on dj.StudioID = d.studioid inner join dvds dv on dv.dvdid = dj.dvdid where dv.dvdid = @id", new { @id = id }).FirstOrDefault();
                return studio;
            }
        }
    }
}

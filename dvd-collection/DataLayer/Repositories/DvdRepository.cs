﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DataLayer.Config;
using DataLayer.Data;

namespace DataLayer.Repositories
{
    public class DvdRepository
    {
        public List<Dvd> GetAll()
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var dvds = cn.Query<Dvd>("Select * from Dvds").ToList();
                return dvds;
            }
        }

        public void AddDvd(Dvd dvd)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var p = new DynamicParameters();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "AddDvd";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@Title", dvd.Title);
                cmd.Parameters.AddWithValue("@ReleaseDate", dvd.ReleaseDate);
                cmd.Parameters.AddWithValue("@Details", dvd.Details);
                cmd.Parameters.AddWithValue("@ImageLink", dvd.ImageLink);
                cmd.Parameters.AddWithValue("@RatingID", dvd.RatingID);

                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        public Dvd GetDvd(int id)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var dvd = cn.Query<Dvd>("Select * from Dvds where DvdID = @DvdID", new {DvdID = id}).FirstOrDefault();
                return dvd;
            }
        }

        public int GetDvdId(string name)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var id = cn.Query<Dvd>("Select dvdid from dvds where dvds.title = @name", new {@name = name}).FirstOrDefault();
                return id.DvdID;
            }
        }

        public List<Actor> GetActors(int id)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var actors = cn.Query<Actor>("select * from dvds d join actorjunction aj on aj.dvdid=d.dvdid join actors a on a.actorid = aj.actorid where d.dvdid = @dvdid", new {@dvdid = id}).ToList();
                return actors;
            }
        }
    }
}

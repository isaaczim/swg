﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DataLayer.Config;
using DataLayer.Data;

namespace DataLayer.Repositories
{
    public class ActorsRepository
    {
        public List<Actor> GetAllActors()
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var actors = cn.Query<Actor>("Select * from actors");
                return actors.ToList();
            }
        }

        public void AddActor(Actor actor)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                cn.Execute("Insert actors(name) values(@Name)", new { @Name = actor.Name });
            }
        }

        public void AddDvd(int actorId, int dvdId)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("dvdId", dvdId);
                p.Add("ActorId", actorId);
                cn.Execute("Insert actorjunction(dvdid, actorid) values(@dvdId, @ActorId)",
                    p);
            }
        }

        public Actor GetActor(int id)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var actor = cn.Query<Actor>(
                    "Select name from actors d inner join actorjunction dj on dj.ActorID = d.actorid inner join dvds dv on dv.dvdid = dj.dvdid where dv.dvdid = @id", new { @id = id }).FirstOrDefault();
                return actor;
            }
        }

        public List<Actor> GetActorsDvd(int id)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var actors =
                    cn.Query<Actor>(
                        "select * from actors a join actorjunction aj on aj.actorid = a.actorid where dvdid = @id",
                        new {@id = id}).ToList();
                return actors;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class Logic
    {
        public bool Turn(int turn, string playerName, char[,] boardPositions, bool[,] positionAvailable, char xOro)
        {
            //Initializing Variables
            DrawBoard _draw = new DrawBoard();
            Console.Write($"{playerName} please choose a position: ");
            string playerInput = Console.ReadLine();
            bool retry = true;
            int position = 0;
            int boardCount = 1;

            //Check if the chosen position is available
            do
            {
                if (int.TryParse(playerInput, out position))
                {
                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            if (positionAvailable[i,j] && boardCount == position)
                            {
                                positionAvailable[i,j] = false;
                                boardPositions[i, j] = xOro;
                                retry = false;
                            }
                            boardCount++;
                        }
                    } 
                }
                if (retry)
                {
                    Console.Write($"{playerName} Please enter a valid position: ");
                    playerInput = Console.ReadLine();
                }
            } while (retry);

            return CheckWin(boardPositions);
        }


        private bool CheckWin(char [,] boardPositions)
        {
            //Check Win States
            for (int i = 0; i < 3; i++)
            {
                if (boardPositions[0,i] == boardPositions[1, i] && boardPositions[1,i] == boardPositions[2, i])
                    return true;
                if (boardPositions[i, 0] == boardPositions[i, 1] && boardPositions[i, 1] == boardPositions[i, 2])
                    return true;
            }
            if (boardPositions[0, 0] == boardPositions[1, 1] && boardPositions[1, 1] == boardPositions[2, 2])
                return true;
            if (boardPositions[0, 2] == boardPositions[1, 1] && boardPositions[1, 1] == boardPositions[2, 0])
                return true;
            return false;
        }
    }
}

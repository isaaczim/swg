﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class DrawBoard
    {
        public void Draw(char[,] boardText)
        {
            Console.Clear();
            Console.WriteLine(" *******  TicTacToe  *******\n");
            Console.WriteLine("           |     |       ");
            Console.WriteLine("        {0}  |  {1}  |  {2}    ", boardText[0, 0], boardText[0, 1], boardText[0, 2]);
            Console.WriteLine("           |     |       ");
            Console.WriteLine("      -------------------");
            Console.WriteLine("           |     |       ");
            Console.WriteLine("        {0}  |  {1}  |  {2}    ", boardText[1, 0], boardText[1, 1], boardText[1, 2]);
            Console.WriteLine("           |     |       ");
            Console.WriteLine("      -------------------");
            Console.WriteLine("           |     |       ");
            Console.WriteLine("        {0}  |  {1}  |  {2}    ", boardText[1, 0], boardText[1, 1], boardText[1, 2]);
            Console.WriteLine("           |     |       ");
        }
    }
}

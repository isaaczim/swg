﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Program
    {
        static void Main(string[] args)
        {
            //Initialize Variables
            DrawBoard _draw = new DrawBoard();
            Logic _logic = new Logic();
            bool[,] isAvailable = new bool[3,3];
            char[,] boardPositions = new char[3,3];
            int index = 1;
            int turn = 1;
            bool p1Win = false;
            bool p2Win = false;

            //Initialize Boards
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    boardPositions[i, j] = char.Parse(index.ToString());
                    index++;
                }
            }
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    isAvailable[i, j] = true;
                }
            }

            //Get player names
            Console.WriteLine("*******  TicTacToe  *******\n");
            Console.Write("Please Enter P1 Name: ");
            string p1 = Console.ReadLine();
            Console.WriteLine();
            Console.Write("Thank You. Please Enter P2 Name: ");
            string p2 = Console.ReadLine();
            Console.Clear();

            //Run game until a win condition is met or tie
            do
            {
                _draw.Draw(boardPositions);
                if (turn%2 == 0)
                {
                    p2Win = _logic.Turn(turn, p2, boardPositions, isAvailable, 'O');
                }
                else if (turn%2 != 0)
                {
                    p1Win = _logic.Turn(turn, p1, boardPositions, isAvailable, 'X');
                }
                turn++;
            } while (turn < 10 && !p1Win && !p2Win);
            _draw.Draw(boardPositions);

            //Congradulate winner or display tie message
            if (p2Win)
                Console.WriteLine($"{p2} Wins!");
            else if (p1Win)
                Console.WriteLine($"{p1} Wins!");
            else
            Console.WriteLine("Too bad, it's a tie\nPress any key to quit");
            Console.ReadKey();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DieExercise
{
    class Program
    {
        static void Main(string[] args)
        {
            RollDie();
        }

        static void RollDie()
        {
            Random rnd = new Random();
            int[] rolls = new int[6];

            for (int i = 0; i < 100; i++)
            {
                int dieNumber = rnd.Next(0, 7);
                switch (dieNumber)
                {
                    case 1:
                        rolls[0]++;
                        break;
                    case 2:
                        rolls[1]++;
                        break;
                    case 3:
                        rolls[2]++;
                        break;
                    case 4:
                        rolls[3]++;
                        break;
                    case 5:
                        rolls[4]++;
                        break;
                    default:
                        rolls[5]++;
                        break;
                }

            }
            for (int i = 0; i < rolls.Length; i++)
            {
                Console.WriteLine($"{i+1}'s: \t {rolls[i]}\n");
            }
            Console.ReadKey();
        }
    }
}

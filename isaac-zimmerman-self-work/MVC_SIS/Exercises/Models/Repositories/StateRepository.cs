﻿using Exercises.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Exercises.Models.Repositories
{
    public class StateRepository
    {
        private static List<State> _states;

        static StateRepository()
        {
            // sample data
            _states = new List<State>
            {
                new State { StateAbbreviation="KY", StateName="Kentucky", StateId = 1},
                new State { StateAbbreviation="MN", StateName="Minnesota", StateId = 2},
                new State { StateAbbreviation="OH", StateName="Ohio", StateId = 3},
            };
        }

        public static IEnumerable<State> GetAll()
        {
            return _states;
        }

        public static State Get(int stateId)
        {
            return _states.FirstOrDefault(c => c.StateId == stateId);
        }

        public static void Add(State state)
        {
            state.StateId = _states.Max(c=>c.StateId) + 1;
            _states.Add(state);
        }

        public static void Edit(State state)
        {
            var selectedState = _states.FirstOrDefault(c => c.StateId == state.StateId);

            selectedState.StateName = state.StateName;
            selectedState.StateAbbreviation = state.StateAbbreviation;
        }

        public static void Delete(int stateId)
        {
            _states.RemoveAll(c => c.StateId == stateId);
        }
    }
}
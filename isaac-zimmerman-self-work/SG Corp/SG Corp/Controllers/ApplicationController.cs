﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SG_Corp.Models.Data;
using SG_Corp.Models.Repositories;
using SG_Corp.Models.ViewModels;

namespace SG_Corp.Controllers
{
    public class ApplicationController : Controller
    {
        // GET: Application
        public ActionResult Index()
        {
            var _jobs = JobRepository.GetAll();
            return View(_jobs.ToList());
        }

        public ActionResult Apply(int id)
        {
            var viewModel = new JobVM();
            viewModel.Job = JobRepository.Get(id);
            viewModel.Applicant = new Applicant();
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Apply(JobVM model)
        {
            JobRepository.AddApplicant(model.Job, model.Applicant);
            return RedirectToAction("Index");
        }

    }
}
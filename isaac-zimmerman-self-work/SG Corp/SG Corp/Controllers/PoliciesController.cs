﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SG_Corp.Models.Data;
using SG_Corp.Models.Repositories;
using SG_Corp.Models.ViewModels;

namespace SG_Corp.Controllers
{
    public class PoliciesController : Controller
    {
        public ActionResult ViewCategories()
        {
            var categories = CategoryRepository.GetAll();
            return View(categories.ToList());
        }

        public ActionResult ViewPolicies(int id)
        {
            var categories = CategoryRepository.Get(id);
            return View(categories);
        }

        [HttpGet]
        public ActionResult AddPolicy(int id)
        {
            var policyVM = new PolicyVM() {Policy = new Policy(), Category = CategoryRepository.Get(id)};
            return View(policyVM);
        }

        [HttpPost]
        public ActionResult AddPolicy(PolicyVM policyVM)
        {
            CategoryRepository.AddPolicy(policyVM.Category, policyVM.Policy);
            return RedirectToAction("ViewPolicies", new {id = policyVM.Category.CategoryId});
        }

        [HttpGet]
        public ActionResult EditPolicy(Category category, int id)
        {
            var policy = CategoryRepository.GetPolicy(category, id);
            return View(policy);
        }

        [HttpPost]
        public ActionResult EditPolicy(PolicyVM policyVM)
        {
            CategoryRepository.EditPolicy(policyVM.Policy);
            return RedirectToAction("ViewPolicies", new { id = policyVM.Category.CategoryId });
        }

        [HttpGet]
        public ActionResult DeletePolicy(Category category, int id)
        {
            var policy = CategoryRepository.GetPolicy(category, id);
            return View(policy);
        }

        [HttpPost]
        public ActionResult DeletePolicy(PolicyVM policyVM)
        {
            CategoryRepository.Delete(policyVM.Policy.PolicyId);
            return RedirectToAction("ViewPolicies", new { id = policyVM.Category.CategoryId });
        }


        public ActionResult Edit()
        {
            return View();
        }

        public ActionResult EditCategories()
        {
            return View();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SG_Corp.Models.Data;
using SG_Corp.Models.Repositories;

namespace SG_Corp.Controllers
{
    public class AdminController : Controller
    {
        public ActionResult ViewApplicants(int id)
        {
            var applicants = JobRepository.GetApplicants(id);
            return View(applicants);
        }

        public ActionResult ApplicantInfo(Applicant id)
        {
            return View(id);
        }

        [HttpGet]
        public ActionResult Jobs()
        {
            var model = JobRepository.GetAll();
            return View(model.ToList());
        }

        [HttpGet]
        public ActionResult AddJob()
        {
            return View(new Job());
        }

        [HttpPost]
        public ActionResult AddJob(Job job)
        {
            JobRepository.Add(job);
            return RedirectToAction("Jobs");
        }

        [HttpGet]
        public ActionResult JobApplications(int id)
        {
            var applicants = JobRepository.Get(id).Applicants;
            return View(applicants);
        }

        [HttpGet]
        public ActionResult EditJob(int id)
        {
            var job = JobRepository.Get(id);
            return View(job);
        }

        [HttpPost]
        public ActionResult EditJob(Job job)
        {
            JobRepository.Edit(job);
            return RedirectToAction("Jobs");
        }

        [HttpGet]
        public ActionResult DeleteJob(int id)
        {
            var job = JobRepository.Get(id);
            return View(job);
        }

        [HttpPost]
        public ActionResult DeleteJob(Job job)
        {
            JobRepository.Delete(job.JobId);
            return RedirectToAction("Jobs");
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SG_Corp.Models.Data;

namespace SG_Corp.Models.Repositories
{
    public class JobRepository
    {
        private static List<Job> _jobs;

        static JobRepository()
        {
            // sample data
            _jobs = new List<Job>
                {
                    new Job
                    {
                        JobId=1,
                        JobName ="PR",
                        Applicants = new List<Applicant>
                        {
                            new Applicant()
                            {
                                Address = new Address()
                                {
                                    AddressId = 1,
                                    State = "Kansas",
                                    City = "Manhattan",
                                    PostalCode = "66502",
                                    Street1 = "401 Summit Ave."
                                },
                                Email = "izimmerman11@gmail.com",
                                Name = "Isaac Zimmerman",
                                Phone = "6209600035",
                                OtherInfo = "Highly qualified.",
                                id = 1
                            }
                        }
                    },
                    new Job { JobId=2, JobName="Marketing" },
                    new Job { JobId=3, JobName="IT" }
                };
        }

        public static IEnumerable<Job> GetAll()
        {
            return _jobs;
        }

        public static List<Applicant> GetApplicants(int id)
        {
            var jobFound = _jobs.FirstOrDefault(c => c.JobId == id);
            return jobFound.Applicants;
        }

        public static void AddApplicant(Job job, Applicant applicant)
        {
            var selectedJob = _jobs.First(c => c.JobId == job.JobId);
            if (selectedJob.Applicants != null)
            {
                applicant.id = selectedJob.Applicants.Max(c => c.id) + 1;
            }
            else
            {
                selectedJob.Applicants = new List<Applicant>();
                applicant.id = 1;
            }

            selectedJob.Applicants.Add(applicant);
        }

        public static Job Get(int jobId)
        {
            return _jobs.FirstOrDefault(c => c.JobId == jobId);
        }

        public static void Add(Job _job)
        {
            Job job = new Job();
            job.JobName = _job.JobName;
            job.Description = _job.Description;
            job.JobId = _jobs.Max(c => c.JobId) + 1;

            _jobs.Add(job);
        }

        public static void Edit(Job job)
        {
            var selectedJob = _jobs.First(c => c.JobId == job.JobId);

            selectedJob.JobName = job.JobName;
            selectedJob.JobStatus = job.JobStatus;
            selectedJob.Description = job.Description;
        }

        public static void Delete(int jobId)
        {
            _jobs.RemoveAll(c => c.JobId == jobId);
        }
    }
}
﻿using SG_Corp.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SG_Corp.Models.Repositories
{
    public class CategoryRepository
    {
        private static List<Category> _categories;

        static CategoryRepository()
        {
            // sample data
            _categories = new List<Category>
                {
                    new Category
                    {
                        CategoryId=1,
                        CategoryName ="Fun Times",
                        Policies = new List<Policy>
                        {
                            new Policy()
                            {
                                PolicyId = 1,
                                Name = "Fun",
                                link = "www.google.com"
                            }
                        }
                    },
                    new Category { CategoryId=2, CategoryName="IT" }
                };
        }

        public static void EditPolicy(Policy policy)
        {
            
        }

        public static IEnumerable<Category> GetAll()
        {
            return _categories;
        }

        public static List<Policy> GetPolicies(int id)
        {
            var categoryFound = _categories.FirstOrDefault(c => c.CategoryId == id);
            return categoryFound.Policies;
        }

        public static Policy GetPolicy(Category category, int id)
        {
            var newCat = _categories.First(c => c.CategoryId == category.CategoryId);
            return newCat.Policies[id];
        }

        public static void AddPolicy(Category category, Policy policy)
        {
            var selectedCategory = _categories.First(c => c.CategoryId == category.CategoryId);
            if (selectedCategory.Policies != null)
            {
                policy.PolicyId = selectedCategory.Policies.Max(c => c.PolicyId) + 1;
            }
            else
            {
                selectedCategory.Policies = new List<Policy>();
                policy.PolicyId = 1;
            }

            selectedCategory.Policies.Add(policy);
        }

        public static Category Get(int categoryId)
        {
            return _categories.FirstOrDefault(c => c.CategoryId == categoryId);
        }

        public static void Add(Category _category)
        {
            Category category = new Category();
            category.CategoryName = _category.CategoryName;
            
            category.CategoryId = _categories.Max(c => c.CategoryId) + 1;

            _categories.Add(category);
        }

        public static void Edit(Category category)
        {
            var selectedCategory = _categories.First(c => c.CategoryId == category.CategoryId);

            selectedCategory.CategoryName = category.CategoryName;
        }

        public static void Delete(int categoryId)
        {
            _categories.RemoveAll(c => c.CategoryId == categoryId);
        }
    }
}
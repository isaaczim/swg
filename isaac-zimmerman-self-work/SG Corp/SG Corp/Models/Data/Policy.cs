﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SG_Corp.Models.Data
{
    public class Policy
    {
        public int PolicyId { get; set; }
        public string link { get; set; }
        public string Name { get; set; }
    }
}
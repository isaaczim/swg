﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SG_Corp.Models.Data
{
    public class Job
    {
        public int JobId { get; set; }
        public string JobName { get; set; }
        public bool JobStatus { get; set; }
        public string Description { get; set; }
        public List<Applicant> Applicants { get; set; }
    }
}
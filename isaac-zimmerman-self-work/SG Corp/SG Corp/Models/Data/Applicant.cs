﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Web;

namespace SG_Corp.Models.Data
{
    public class Applicant
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string OtherInfo { get; set; }
        public Address Address { get; set; }
        public int id { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SG_Corp.Models.Data
{
    public class State
    {
        public string StateAbbreviation { get; set; }
        public string StateName { get; set; }
        public int StateId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SG_Corp.Models.Data
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public List<Policy> Policies { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SG_Corp.Models.Data;

namespace SG_Corp.Models.ViewModels
{
    public class PolicyVM
    {
        public Policy Policy { get; set; }
        public Category Category { get; set; }
    }
}
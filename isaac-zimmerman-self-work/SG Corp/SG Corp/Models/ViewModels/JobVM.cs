﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SG_Corp.Models.Data;
using SG_Corp.Models.Repositories;

namespace SG_Corp.Models.ViewModels
{
    public class JobVM
    {
        public Job Job { get; set; }
        public Applicant Applicant { get; set; }
    }
}
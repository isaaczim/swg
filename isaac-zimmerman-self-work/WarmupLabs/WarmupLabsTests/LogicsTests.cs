﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using WarmupLabs;

namespace WarmupLabsTests
{
    [TestFixture]
    class LogicsTests
    {
        private Logics _logics;

        [SetUp]
        public void BeforeEachTest()
        {
            _logics = new Logics();
        }

        [TestCase(30, false, false)]
        [TestCase(50, false, true)]
        [TestCase(70, true, true)]
        public void SayHiReturnsExpected(int a, bool b, bool expected)
        {
            bool result = _logics.GreatParty(a, b);

            Assert.AreEqual(expected, result);
        }

        [TestCase(5, 10, 2)]
        [TestCase(5, 2, 0)]
        [TestCase(5, 5, 1)]
        public void CanHazTableReturnsExpected(int a, int b, int expected)
        {
            int result = _logics.CanHazTable(a, b);

            Assert.AreEqual(expected, result);
        }

        [TestCase(70, false, true)]
        [TestCase(95, false, false)]
        [TestCase(95, true, true)]
        public void PlayOutsideReturnsExpected(int a, bool b, bool expected)
        {
            bool result = _logics.PlayOutside(a, b);

            Assert.AreEqual(expected, result);
        }

        [TestCase(60, false, 0)]
        [TestCase(65, false, 1)]
        [TestCase(65, true, 0)]
        public void CaughtSpeedingReturnsExpected(int a, bool b, int expected)
        {
            int result = _logics.CaughtSpeeding(a, b);

            Assert.AreEqual(expected, result);
        }

        [TestCase(3, 4, 7)]
        [TestCase(9, 4, 20)]
        [TestCase(10, 11, 21)]
        public void SkipSumReturnsExpected(int a, int b, int expected)
        {
            int result = _logics.SkipSum(a, b);

            Assert.AreEqual(expected, result);
        }

        [TestCase(1, false, "7:00")]
        [TestCase(5, false, "7:00")]
        [TestCase(0, false, "10:00")]
        public void AlarmClockReturnsExpected(int a, bool b, string expected)
        {
            string result = _logics.AlarmClock(a, b);

            Assert.AreEqual(expected, result);
        }

        [TestCase(6, 4, true)]
        [TestCase(4, 5, false)]
        [TestCase(1, 5, true)]
        public void LoveSixReturnsExpected(int a, int b, bool expected)
        {
            bool result = _logics.LoveSix(a, b);

            Assert.AreEqual(expected, result);
        }

        [TestCase(5, false, true)]
        [TestCase(11, false, false)]
        [TestCase(11, true, true)]
        public void InRangeReturnsExpected(int a, bool b, bool expected)
        {
            bool result = _logics.InRange(a, b);

            Assert.AreEqual(expected, result);
        }

        [TestCase(22, true)]
        [TestCase(23, true)]
        [TestCase(24, false)]
        public void Special11ReturnsExpected(int a, bool expected)
        {
            bool result = _logics.Special11(a);

            Assert.AreEqual(expected, result);
        }

        [TestCase(10, false)]
        [TestCase(21, true)]
        [TestCase(22, true)]
        public void Mod20ReturnsExpected(int a, bool expected)
        {
            bool result = _logics.Mod20(a);

            Assert.AreEqual(expected, result);
        }

        [TestCase(3, true)]
        [TestCase(10, true)]
        [TestCase(15, false)]
        public void Mod35ReturnsExpected(int a, bool expected)
        {
            bool result = _logics.Mod35(a);

            Assert.AreEqual(expected, result);
        }

        [TestCase(false, false, false, true)]
        [TestCase(false, false, true, false)]
        [TestCase(true, false, false, false)]
        public void AnswerCell(bool a, bool b, bool c, bool expected)
        {
            bool returned = _logics.AnswerCell(a, b, c);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(1, 2, 3, true)]
        [TestCase(3, 1, 2, true)]
        [TestCase(3, 2, 2, false)]
        public void TwoIsOneReturnsExpected(int a, int b, int c, bool expected)
        {
            bool result = _logics.TwoIsOne(a, b, c);

            Assert.AreEqual(expected, result);
        }

        [TestCase(1, 2, 4, false, true)]
        [TestCase(1, 2, 1, false, false)]
        [TestCase(1, 1, 2, true, true)]
        public void AreIneOrderReturnsExpected(int a, int b, int c, bool d, bool expected)
        {
            bool result = _logics.AreInOrder(a, b, c, d);

            Assert.AreEqual(expected, result);
        }

        [TestCase(2, 5, 11, false, true)]
        [TestCase(5, 7, 6, false, false)]
        [TestCase(5, 5, 7, true, true)]
        public void InOrderEqualReturnsExpected(int a, int b, int c, bool d, bool expected)
        {
            bool result = _logics.InOrderEqual(a, b, c, d);

            Assert.AreEqual(expected, result);
        }

        [TestCase(23, 19, 13, true)]
        [TestCase(23, 19, 12, false)]
        [TestCase(23, 19, 3, true)]
        public void LastDigitReturnsExpected(int a, int b, int c, bool expected)
        {
            bool result = _logics.LastDigit(a, b, c);

            Assert.AreEqual(expected, result);
        }

        [TestCase(2, 3, true, 5)]
        [TestCase(3, 3, true, 7)]
        [TestCase(3, 3, false, 6)]
        public void RollDiceReturnsExpected(int die1, int die2, bool noDoubles, int expected)
        {
            int result = _logics.RollDice(die1, die2, noDoubles);

            Assert.AreEqual(expected, result);
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WarmupLabs;

namespace WarmupLabsTests
{
    [TestFixture]
    class LoopsTests
    {
        private Loops _loops;

        [SetUp]
        public void BeforeEachTest()
        {
            _loops = new Loops();
        }

        [TestCase("Hi", 2, "HiHi")]
        [TestCase("Hi", 3, "HiHiHi")]
        [TestCase("Hi", 1, "Hi")]
        public void StringTimesReturnsExpected(string a, int b, string expected)
        {
            string returned = _loops.StringTimes(a, b);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("Choclolate", 2, "ChoCho")]
        [TestCase("Chocolate", 3, "ChoChoCho")]
        [TestCase("Abc", 3, "AbcAbcAbc")]
        public void FrontTimesReturnsExpected(string a, int b, string expected)
        {
            string returned = _loops.FrontTimes(a, b);
            
            Assert.AreEqual(expected, returned);
        }

        [TestCase("abcxx", 1)]
        [TestCase("xxx", 2)]
        [TestCase("xxxx", 3)]
        [TestCase("x", 0)]
        public void CountXXReturnsExpected(string input, int expected)
        {
            int returned = _loops.CountXX(input);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("axxbb", true)]
        [TestCase("axaxxax", false)]
        [TestCase("xxxxx", true)]
        [TestCase("", false)]
        public void DoubleXReturnsExpected(string input, bool expected)
        {
            bool returned = _loops.DoubleX(input);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("Hello", "Hlo")]
        [TestCase("Hi","H")]
        [TestCase("Heeololeo", "Hello")]
        [TestCase("", "")]
        public void EveryOtherReturnsExpected(string input, string expected)
        {
            string returned = _loops.EveryOther(input);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("Code", "CCoCodCode")]
        [TestCase("abc", "aababc")]
        [TestCase("ab", "aab")]
        [TestCase("", "")]
        public void StringSplosionReturnsExpected(string input, string expected)
        {
            string returned = _loops.StringSplosion(input);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("hixxhi", 1)]
        [TestCase("xaxxaxaxx", 1)]
        [TestCase("axxxaxx", 2)]
        [TestCase("", 0)]
        public void CountLast2ReturnsExpected(string input, int expected)
        {
            int returned = _loops.CountLast2(input);

            Assert.AreEqual(expected, returned);
        }
         
        [TestCase(new int[] { 1, 2, 9 }, 1)]
        [TestCase(new int[] { 1, 9, 9 }, 2)]
        [TestCase(new int[] { 1, 9, 9, 9, 3}, 3)]
        public void Count9ReturnsExpected(int[] input, int expected)
        {
            int returned = _loops.Count9(input);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(new int[] { 1, 2, 9, 3, 4 }, true)]
        [TestCase(new int[] { 1, 2, 3, 4, 9 }, false)]
        [TestCase(new int[] { 1, 2, 3, 9, 5 }, true)]
        [TestCase(new int[] { 1, 2, 3 }, false)]
        public void ArrayFront9ReturnsExpected(int[] input, bool expected)
        {
            bool returned = _loops.ArrayFront9(input);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(new int[] { 1, 1, 2, 3, 1 }, true)]
        [TestCase(new int[] { 1, 1, 2, 4, 1 }, false)]
        [TestCase(new int[] { 1, 1, 2, 1, 2 , 3 }, true)]
        public void Array123ReturnsExpected(int[] input, bool expected)
        {
            bool returned = _loops.Array123(input);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("xxcaazz", "xxbaaz", 3)]
        [TestCase("abc", "abc", 2)]
        [TestCase("abc", "axc", 0)]
        public void SubStringMatchReturnsExpected(string a, string b, int expected)
        {
            int returned = _loops.SubStringMatch(a, b);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("xxHxix", "xHix")]
        [TestCase("abxxxcd", "abcd")]
        [TestCase("xabxxxcdx", "xabcdx")]
        public void StringXReturnsExpected(string input, string expected)
        {
            string returned = _loops.StringX(input);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("kitten", "kien")]
        [TestCase("Chocolate", "Chole")]
        [TestCase("CodingHorror", "Congrr")]
        [TestCase("", "")]
        public void AltPairsReturnsExpected(string input, string expected)
        {
            string returned = _loops.AltPairs(input);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("yakpak", "pak")]
        [TestCase("pakyak", "pak")]
        [TestCase("yak123ya", "123ya")]
        public void DoNotYakReturnsExpected(string input, string expected)
        {
            string returned = _loops.DoNotYak(input);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(new int[] { 6, 6, 2 }, 1)]
        [TestCase(new int[] { 6, 6, 2, 6 }, 1)]
        [TestCase(new int[] { 6, 7, 2, 6 }, 1)]
        public void Array667ReturnsExpected(int[] input, int expected)
        {
            int returned = _loops.Array667(input);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(new int[] { 1, 1, 2, 2, 1 }, true)]
        [TestCase(new int[] { 1, 1, 2, 2, 2, 1 }, false)]
        [TestCase(new int[] { 1, 1, 1, 2, 2, 2, 1 }, false)]
        public void NoTriplesReturnsExpected(int[] input, bool expected)
        {
            bool returned = _loops.NoTriples(input);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(new int[] { 1, 2, 7, 1 }, true)]
        [TestCase(new int[] { 1, 2, 8, 1 }, false)]
        [TestCase(new int[] { 2, 7, 1 }, true)]
        public void Pattern51ReturnsExpected(int[] input, bool expected)
        {
            bool returned = _loops.Pattern51(input);

            Assert.AreEqual(expected, returned);
        }

    }
}

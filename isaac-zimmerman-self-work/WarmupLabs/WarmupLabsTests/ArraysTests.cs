﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WarmupLabs;

namespace WarmupLabsTests
{
    [TestFixture]
    public class ArraysTests
    {
        private Arrays _arrays;

        [SetUp]
        public void BeforeEachTest()
        {
            _arrays = new Arrays();
        }

        [TestCase(new int[] {1, 2, 6}, true)]
        [TestCase(new int[] {6, 1, 2, 3}, true)]
        [TestCase(new int[] {13, 6, 1, 2, 3}, false)]
        public void Return6ReturnsExpected(int[] input, bool expected)
        {
            bool result = _arrays.FirstLast6(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] { 1, 2, 3 }, false)]
        [TestCase(new int[] { 1, 2, 3, 1 }, true)]
        [TestCase(new int[] { 1, 2, 1 }, true)]
        public void SameFirstLastReturnsExpected(int[] input, bool expected)
        {
            bool result = _arrays.SameFirstLast(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase(3, new int[] { 3, 1, 4 })]
        public void MakePiReturnsExpected(int input, int[] expected)
        {
            int[] result = new int[input];
            result = _arrays.MakePi(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] { 1, 2, 3 }, new int[] {7,3}, true)]
        [TestCase(new int[] { 1, 2, 3 }, new int[] {7,3,2}, false)]
        [TestCase(new int[] { 1, 2, 3 }, new int[] {1,3}, true)]
        public void CommonEndReturnsExpected(int[] a, int[]b, bool expected)
        {
            bool result = _arrays.commonEnd(a,b);

            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] { 1, 2, 3 }, 6)]
        [TestCase(new int[] { 5, 11, 2 }, 18)]
        [TestCase(new int[] { 7, 0, 0 }, 7)]
        public void SumReturnsExpected(int[] input, int expected)
        {
            int result = _arrays.Sum(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] { 1, 2, 3 }, new int[] {2,3,1})]
        [TestCase(new int[] { 5, 11, 9 }, new int[] {11,9,5})]
        [TestCase(new int[] { 7, 0, 0 }, new int[] {0,0,7})]
        public void RotateLeftReturnsExpected(int[] input, int[] expected)
        {
            int[] result = new int[input.Length];
            result = _arrays.RotateLeft(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] { 1, 2, 3 }, new int[] { 3, 2, 1 })]
        [TestCase(new int[] { 5, 11, 9 }, new int[] { 9, 11, 5 })]
        [TestCase(new int[] { 7, 0, 0 }, new int[] { 0, 0, 7 })]
        public void ReverseReturnsExpected(int[] input, int[] expected)
        {
            int[] result = new int[input.Length];
            result = _arrays.Reverse(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] { 1, 2, 3 }, new int[] { 3, 3, 3 })]
        [TestCase(new int[] { 5, 9, 11 }, new int[] { 11, 11, 11 })]
        [TestCase(new int[] { 2, 11, 3 }, new int[] { 3, 3, 3 })]
        public void HigherWinsReturnsExpected(int[] input, int[] expected)
        {
            int[] result = new int[input.Length];
            result = _arrays.HigherWins(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] { 1, 2, 3 }, new int[] { 4, 5, 6 }, new int[] {2,5})]
        [TestCase(new int[] { 7, 7, 7 }, new int[] { 3, 8, 0 }, new int[] {7,8})]
        [TestCase(new int[] { 2, 11, 3 }, new int[] { 3, 3, 3 }, new int[] {11,3})]
        public void GetMiddleReturnsExpected(int[] a, int[] b, int[] expected)
        {
            int[] result = new int[a.Length];
            result = _arrays.GetMiddle(a,b);

            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] { 2, 5 }, true)]
        [TestCase(new int[] { 4, 3 }, true)]
        [TestCase(new int[] { 7, 5 }, false)]
        public void HasEvenReturnsExpected(int[] input, bool expected)
        {
            bool result = _arrays.HasEven(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] { 1, 2, 3 }, new int[] { 0, 0, 0, 0, 0, 3 })]
        [TestCase(new int[] { 5, 11 }, new int[] { 0, 0, 0, 11 })]
        [TestCase(new int[] { 2, 11, 3, 4 }, new int[] { 0, 0, 0, 0, 0, 0, 0, 4 })]
        public void KeepLastReturnsExpected(int[] input, int[] expected)
        {
            int[] result = new int[input.Length];
            result = _arrays.KeepLast(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] { 2, 2, 3 }, true)]
        [TestCase(new int[] { 3, 4, 5, 3 }, true)]
        [TestCase(new int[] { 2, 3, 2, 2 }, false)]
        public void Double23ReturnsExpected(int[] input, bool expected)
        {
            bool result = _arrays.Double23(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] { 1, 2, 3 }, new int[] { 1, 2, 0 })]
        [TestCase(new int[] { 2, 3, 5 }, new int[] { 2, 0, 5 })]
        [TestCase(new int[] { 1, 2, 1 }, new int[] { 1, 2, 1 })]
        public void Fix23ReturnsExpected(int[] input, int[] expected)
        {
            int[] result = new int[input.Length];
            result = _arrays.Fix23(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] { 1, 3, 4, 5 }, true)]
        [TestCase(new int[] { 2, 1, 3, 4, 5 }, true)]
        [TestCase(new int[] { 1, 1, 1 }, false)]
        public void Unlucky1ReturnsExpected(int[] input, bool expected)
        {
            bool result = _arrays.Unlucky1(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] { 4,5 }, new int[] { 1,2,3 }, new int[] { 4,5 })]
        [TestCase(new int[] { 4 }, new int[] { 1,2 }, new int[] { 4,1 })]
        [TestCase(new int[] {  }, new int[] { 1,2 }, new int[] { 1,2 })]
        public void Make2ReturnsExpected(int[] a, int[] b, int[] expected)
        {
            int[] result = new int[a.Length];
            result = _arrays.make2(a, b);

            Assert.AreEqual(expected, result);
        }

    }
}

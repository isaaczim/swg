﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using WarmupLabs;

namespace WarmupLabsTests
{
    [TestFixture]
    class ConditionalsTests
    {
        private Conditionals _conditionals;

        [SetUp]
        public void BeforeEachTest()
        {
            _conditionals = new Conditionals();
        }

        [TestCase(true, true, true)]
        [TestCase(false, false, true)]
        [TestCase(true, false, false)]
        public void StringTimesReturnsExpected(bool a, bool b, bool expected)
        {
            bool returned = _conditionals.AreWeInTrouble(a, b);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(false, false, true)]
        [TestCase(true, false, false)]
        [TestCase(false, true, true)]
        public void sleepInReturnsExpected(bool a, bool b, bool expected)
        {
            bool returned = _conditionals.SleepIn(a, b);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(1,2,3)]
        [TestCase(3,2,5)]
        [TestCase(2,2,8)]
        public void SumDoubleReturnsExpected(int a, int b, int expected)
        {
            int returned = _conditionals.SumDouble(a, b);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(23, 4)]
        [TestCase(10, 11)]
        [TestCase(21, 0)]
        public void Diff21ReturnsExpected(int a, int expected)
        {
            int returned = _conditionals.Diff21(a);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(true, 6, true)]
        [TestCase(true, 7, false)]
        [TestCase(false, 6, false)]
        public void ParrotTroubleReturnsExpected(bool a, int b, bool expected)
        {
            bool returned = _conditionals.ParrotTrouble(a,b);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(9, 10, true)]
        [TestCase(9, 9, false)]
        [TestCase(1, 9, true)]
        public void Makes10ReturnsExpected(int a, int b, bool expected)
        {
            bool returned = _conditionals.Makes10(a, b);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(103, true)]
        [TestCase(90, true)]
        [TestCase(89, false)]
        public void NearHundredReturnsExpected(int a, bool expected)
        {
            bool returned = _conditionals.NearHundred(a);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(1, -1, false, true)]
        [TestCase(-1, 1, false, true)]
        [TestCase(-21, -10, true, true)]
        public void PosNegReturnsExpected(int a, int b, bool c, bool expected)
        {
            bool returned = _conditionals.PosNeg(a, b, c);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("candy", "not candy")]
        [TestCase("x", "not x")]
        [TestCase("not bad", "not bad")]
        public void NotStringReturnsExpected(string a, string expected)
        {
            string returned = _conditionals.NotString(a);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("kitten", 1, "ktten")]
        [TestCase("kitten", 0, "itten")]
        [TestCase("kitten", 4, "kittn")]
        public void MissingCharReturnsExpected(string a, int n, string expected)
        {
            string returned = _conditionals.MissingChar(a,n);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("code", "eodc")]
        [TestCase("a", "a")]
        [TestCase("ab", "ba")]
        public void FrontBackReturnsExpected(string a, string expected)
        {
            string returned = _conditionals.FrontBack(a);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("Microsoft", "MicMicMic")]
        [TestCase("Chocolate", "ChoChoCho")]
        [TestCase("at", "atatat")]
        public void Front3ReturnsExpected(string a, string expected)
        {
            string returned = _conditionals.Front3(a);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("cat", "tcatt")]
        [TestCase("Hello", "oHelloo")]
        [TestCase("a", "aaa")]
        public void BackAroundReturnsExpected(string a, string expected)
        {
            string returned = _conditionals.BackAround(a);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(3, true)]
        [TestCase(10, true)]
        [TestCase(8, false)]
        public void Multiple3or5ReturnsExpected(int a, bool expected)
        {
            bool returned = _conditionals.Multiple3or5(a);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("hi there", true)]
        [TestCase("hi", true)]
        [TestCase("high up", false)]
        public void StartHiReturnsExpected(string a, bool expected)
        {
            bool returned = _conditionals.StartHi(a);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(120, -1, true)]
        [TestCase(-1, 120, true)]
        [TestCase(2, 120, false)]
        public void IcyHotReturnsExpected(int a, int b, bool expected)
        {
            bool returned = _conditionals.IcyHot(a, b);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(12, -99, true)]
        [TestCase(21, 12, true)]
        [TestCase(8, 99, false)]
        public void Between10and20ReturnsExpected(int a, int b, bool expected)
        {
            bool returned = _conditionals.Between10and20(a, b);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(13, 99, true)]
        [TestCase(21, 19, true)]
        [TestCase(13, 13, false)]
        public void SoAloneReturnsExpected(int a, int b, bool expected)
        {
            bool returned = _conditionals.SoAlone(a, b);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("adelbc", "abc")]
        [TestCase("adelHello", "aHello")]
        [TestCase("adedbc", "adedbc")]
        public void RemoveDelReturnsExpected(string a, string expected)
        {
            string returned = _conditionals.RemoveDel(a);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("mix snacks", true)]
        [TestCase("pix snacks", true)]
        [TestCase("piz snacks", false)]
        public void IxStartReturnsExpected(string a, bool expected)
        {
            bool returned = _conditionals.IxStart(a);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("ozdaef", "oz")]
        [TestCase("bzllkj", "z")]
        [TestCase("o;lsdkjf", "o")]
        public void StartOzReturnsExpected(string a, string expected)
        {
            string returned = _conditionals.StartOz(a);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(1, 2, 3, 3)]
        [TestCase(1, 3, 2, 3)]
        [TestCase(3, 2, 1, 3)]
        public void MaxReturnsExpected(int a, int b, int c, int expected)
        {
            int returned = _conditionals.Max(a, b, c);

            Assert.AreEqual(expected, returned);
        }

        [TestCase(8, 13, 8)]
        [TestCase(13, 8, 8)]
        [TestCase(13, 7, 0)]
        public void CloserReturnsExpected(int a, int b, int expected)
        {
            int returned = _conditionals.Closer(a, b);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("Hello", true)]
        [TestCase("Heelle", true)]
        [TestCase("Heelele", false)]
        public void GotEReturnsExpected(string a, bool expected)
        {
            bool returned = _conditionals.GotE(a);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("Hello", "HeLLO")]
        [TestCase("hi there", "hi thERE")]
        [TestCase("hi", "HI")]
        public void EndUpReturnsExpected(string a, string expected)
        {
            string returned = _conditionals.EndUp(a);

            Assert.AreEqual(expected, returned);
        }

        [TestCase("Miracle", 2, "Mrce")]
        [TestCase("abcdefg", 2, "aceg")]
        [TestCase("abcdefg", 3, "adg")]
        public void EveryNthReturnsExpected(string a, int n, string expected)
        {
            string returned = _conditionals.EveryNth(a, n);

            Assert.AreEqual(expected, returned);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WarmupLabs;

namespace WarmupLabsTests
{
    [TestFixture]
    public class StringsTests
    {
        private Strings _strings;

        [SetUp]
        public void BeforeEachTest()
        {
                _strings = new Strings();
        }

        [TestCase("Bob", "Hello Bob!")]
        [TestCase("Alice", "Hello Alice!")]
        [TestCase("X", "Hello X!")]
        public void SayHiReturnsExpected(string input, string expected)
        {
            string result = _strings.SayHi(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase("Hi", "Bye", "HiByeByeHi")]
        [TestCase("Yo", "Alice", "YoAliceAliceYo")]
        [TestCase("What", "Up", "WhatUpUpWhat")]

        public void AbbaReturnsExpected(string a, string b, string expected)
        {
            string result = _strings.Abba(a, b);

            Assert.AreEqual(expected, result);
        }

        [TestCase("i", "Yay", "<i>Yay</i>")]
        [TestCase("i", "Hello", "<i>Hello</i>")]
        [TestCase("cite", "Yay", "<cite>Yay</cite>")]

        public void TagsReturnsExpected(string a, string b, string expected)
        {
            string result = _strings.Tags(a, b);

            Assert.AreEqual(expected, result);
        }

        [TestCase("<<>>", "Yay", "<<Yay>>")]
        [TestCase("<<>>", "Hello", "<<Hello>>")]
        [TestCase("[[]]", "Yay", "[[Yay]]")]

        public void InsertReturnsExpected(string a, string b, string expected)
        {
            string result = _strings.InsertWord(a, b);

            Assert.AreEqual(expected, result);
        }

        [TestCase("Hello", "lololo")]
        [TestCase("ab", "ababab")]
        [TestCase("Hi", "HiHiHi")]
        public void MultiEndingsReturnsExpected(string input, string expected)
        {
            string result = _strings.MultipleEndings(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase("WooHoo", "Woo")]
        [TestCase("HelloThere", "Hello")]
        [TestCase("abcdef", "abc")]
        public void FirstHalfReturnsExpected(string input, string expected)
        {
            string result = _strings.FirstHalf(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase("Hello", "ell")]
        [TestCase("java", "av")]
        [TestCase("coding", "odin")]
        public void TrimOneReturnsExpected(string input, string expected)
        {
            string result = _strings.TrimOne(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase("Hello", "hi", "hiHellohi")]
        [TestCase("hi", "Hello", "hiHellohi")]
        [TestCase("aaa", "b", "baaab")]

        public void LongInMiddleReturnsExpected(string a, string b, string expected)
        {
            string result = _strings.LongInMiddle(a, b);

            Assert.AreEqual(expected, result);
        }

        [TestCase("Hello", "lloHe")]
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void RotateLeft2ReturnsExpected(string input, string expected)
        {
            string result = _strings.RotateLeft2(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase("Hello", "loHel")]
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void RotateRight2ReturnsExpected(string input, string expected)
        {
            string result = _strings.RotateRight2(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase("Hello", true, "H")]
        [TestCase("Hello", false, "o")]
        [TestCase("oh", true, "o")]

        public void TakeOneReturnsExpected(string a, bool b, string expected)
        {
            string result = _strings.TakeOne(a, b);

            Assert.AreEqual(expected, result);
        }

        [TestCase("string", "ri")]
        [TestCase("java", "av")]
        [TestCase("Practice", "ct")]
        public void MiddleTwoReturnsExpected(string input, string expected)
        {
            string result = _strings.MiddleTwo(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase("oddly", true)]
        [TestCase("od", false)]
        [TestCase("oddy", false)]
        public void EndsWithLyReturnsExpected(string input, bool expected)
        {
            bool result = _strings.EndsWithLy(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase("Hello", 2, "Helo")]
        [TestCase("Chocolate", 3, "Choate")]
        [TestCase("Chocolate", 1, "Ce")]
        public void FrontAndBackReturnsExpected(string a, int b, string expected)
        {
            string result = _strings.FrontAndBack(a, b);

            Assert.AreEqual(expected, result);
        }

        [TestCase("java", 0, "ja")]
        [TestCase("java", 2, "va")]
        [TestCase("java", 3, "ja")]
        public void TakeTwoFromPositionReturnsExpected(string a, int b, string expected)
        {
            string result = _strings.TakeTwoFromPosition(a, b);

            Assert.AreEqual(expected, result);
        }

        [TestCase("badxx", true)]
        [TestCase("xbadxx", true)]
        [TestCase("xxbadxx", false)]
        public void HasBadReturnsExpected(string input, bool expected)
        {
            bool result = _strings.HasBad(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase("hello", "he")]
        [TestCase("hi", "hi")]
        [TestCase("h", "h@")]
        public void AtFirstReturnsExpected(string input, string expected)
        {
            string result = _strings.AtFirst(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase("last", "chars", "ls")]
        [TestCase("yo", "mama", "ya")]
        [TestCase("h", "", "h@")]
        public void LastCharsReturnsExpected(string a, string b, string expected)
        {
            string result = _strings.LastChars(a, b);

            Assert.AreEqual(expected, result);
        }

        [TestCase("abc", "cat", "abcat")]
        [TestCase("dog", "cat", "dogcat")]
        [TestCase("abc", "", "abc")]
        public void ConCatReturnsExpected(string a, string b, string expected)
        {
            string result = _strings.ConCat(a, b);

            Assert.AreEqual(expected, result);
        }

        [TestCase("coding", "codign")]
        [TestCase("cat", "cta")]
        [TestCase("ab", "ba")]
        public void SwapLastReturnsExpected(string input, string expected)
        {
            string result = _strings.SwapLast(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase("edited", true)]
        [TestCase("ed", true)]
        [TestCase("edit", false)]
        [TestCase("e", false)]

        public void FrontAgainReturnsExpected(string input, bool expected)
        {
            bool result = _strings.FrontAgain(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase("Hello", "Hi", "loHi")]
        [TestCase("Hello", "java", "ellojava")]
        [TestCase("java", "Hello", "javaello")]
        public void MinCatReturnsExpected(string a, string b, string expected)
        {
            string result = _strings.MinCat(a, b);

            Assert.AreEqual(expected, result);
        }

        [TestCase("Hello", "llo")]
        [TestCase("away", "aay")]
        [TestCase("", "")]
        public void TweakFrontReturnsExpected(string input, string expected)
        {
            string result = _strings.TweakFront(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase("xHix", "Hi")]
        [TestCase("xHi", "Hi")]
        [TestCase("Hxix", "Hxi")]
        public void StripXReturnsExpected(string input, string expected)
        {
            string result = _strings.StripX(input);

            Assert.AreEqual(expected, result);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WarmupLabs
{
    public class Arrays
    {
        public bool FirstLast6(int[] nums)
        {
            return nums[0] == 6 || nums[nums.Length - 1] == 6;
        }

        public bool SameFirstLast(int[] nums)
        {
            return nums[0] == nums[nums.Length - 1];
        }

        public int[] MakePi(int n)
        {
            int[] r = new int[n];
            string strPi = Math.PI.ToString();
            strPi = strPi.Substring(0, 1) + strPi.Substring(2);
            for (int i = 0; i < n; i++)
            {
                r[i] = int.Parse(strPi[i].ToString());
            }
            return r;
        }

        public bool commonEnd(int[] a, int[] b)
        {
            return (a[0] == b[0] || a[a.Length - 1] == b[b.Length - 1]);
        }

        public int Sum(int[] num)
        {
            int x = 0;
            foreach (var i in num)
            {
                x += i;
            }
            return x;
        }

        public int[] RotateLeft(int[] nums)
        {
            int[] numsReturn = new int[nums.Length];
            for (int i = 0; i < nums.Length-1; i++)
            {
                numsReturn[i] = nums[i + 1];
            }
            numsReturn[numsReturn.Length-1] = nums[0];
           
            return numsReturn;

        }

        public int[] Reverse(int[] nums)
        {
            int count = 0;
            int[] numsReturn = new int[nums.Length];
            for (int i = nums.Length-1; i >= 0; i--)
            {
                numsReturn[i] = nums[count];
                count++;
            }
            return numsReturn;

        }

        public int[] HigherWins(int[] nums)
        {
            int count = 0;
            int[] numsReturn = new int[nums.Length];
            if (nums[0]>nums[nums.Length-1])
            {
                for (int i = 0; i < nums.Length; i++)
                {
                    numsReturn[i] = nums[0];
                }
            }
            for (int i = 0; i < nums.Length; i++)
            {
                numsReturn[i] = nums[nums.Length-1];
            }
            return numsReturn;
        }

        public int[] GetMiddle(int[] a, int[] b)
        {
            int[] newArr = new int[2];
            newArr[0] = a[1];
            newArr[1] = b[1];
            return newArr;
        }

        public bool HasEven(int[] numbers)
        {
            foreach (var number in numbers)
            {
                if (number%2 == 0)
                {
                    return true;
                }
            }
            return false;
        }

        public int[] KeepLast(int[] numbers)
        {
            int[] newArr = new int[numbers.Length*2];
            for (int i = 0; i < newArr.Length-2; i++)
            {
                newArr[i] = 0;
            }
            newArr[newArr.Length - 1] = numbers[numbers.Length - 1];
            return newArr;
        }

        public bool Double23(int[] numbers)
        {
            int count2 = 0;
            int count3 = 0;
            foreach (var number in numbers)
            {
                if (number==2)
                    count2++;
                if (number == 3)
                    count3++;
            }
            return count2 == 2 || count3 == 2;
        }

        public int[] Fix23(int[] numbers)
        {
            int[] nums = new int[numbers.Length];
            for (int i = 0; i < numbers.Length-1; i++)
            {
                if (numbers[i] == 2 && numbers[i+1] == 3)
                {
                    numbers[i + 1] = 0;
                }
            }
            return numbers;
        }

        public bool Unlucky1(int[] numbers)
        {
            for (int i = 0; i < numbers.Length - 2; i++)
            {
                if (numbers[i] == 1 && numbers[i + 1] == 3)
                {
                    if (i > numbers.Length - 2 || i < 2)
                    {
                        return true;
                    }
                }
            }
                return false;

        }

        public int[] make2(int[] a, int[] b)
        {
            int count = 0;
            int[] newArr = new int[2];
            foreach (var num in a)
            {
                if (count < 2)
                {
                    newArr[count] = num;
                    count++;
                }
                
            }
            foreach (var num in b)
            {
                if (count < 2)
                {
                    newArr[count] = num;
                    count++;
                }
                
            }
            return newArr;

        }
    }
}

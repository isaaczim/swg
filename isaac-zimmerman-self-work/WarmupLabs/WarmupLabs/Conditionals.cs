﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarmupLabs
{
    public class Conditionals
    {
        public bool AreWeInTrouble(bool aSmile, bool bSmile)
        {
            return aSmile == bSmile;
        }

        public bool SleepIn(bool isWeekday, bool isVacation)
        {
            return isVacation || !isWeekday;
        }

        public int SumDouble(int a, int b)
        {
            if (a == b)
            {
                return (a + b)*2;
            }
            return a + b;
        }

        public int Diff21(int n)
        {
            if (n > 21)
            {
                return Math.Abs(n - 21)*2;
            }
            return Math.Abs(21 - n);
        }

        public bool ParrotTrouble(bool isTalking, int hour)
        {
            return isTalking && (hour > 20 || hour < 7);
        }

        public bool Makes10(int a, int b)
        {
            return (a == 10 || b == 10 || a + b == 10);
        }

        public bool PosNeg(int a, int b, bool c)
        {
            if (c && a < 0 && b < 0)
                return true;
            return (!c && a > 0 && b < 0) || (!c && a < 0 && b > 0);

        }

        public bool NearHundred(int n)
        {
            return (Math.Abs(n - 100) < 11 || Math.Abs(n - 100) < 11);
        }

        public string NotString(string str)
        {
            if (str.Length > 2 && str[0] == 'n' && str[1] == 'o' && str[2] == 't')
            {
                return str;
            }
            return string.Format($"not {str}");
        }

        public string MissingChar(string str, int n)
        {
            return str.Remove(n, 1);
        }

        public string FrontBack(string str)
        {
            if (str.Length <= 1)
            {
                return str;
            }
            return string.Format($"{str[str.Length-1]}{str.Substring(1,str.Length-2)}{str[0]}");
        }

        public string Front3(string str)
        {
            if (str.Length > 3)
            {
                str = str.Substring(0, 3);
            }
            return string.Format($"{str}{str}{str}");
        }

        public string BackAround(string str)
        {
            return ($"{str[str.Length - 1]}{str}{str[str.Length - 1]}");
        }

        public bool Multiple3or5(int n)
        {
            return n%5 == 0 || n%3 == 0;
        }

        public bool StartHi(string str)
        {
            if (str.Length > 2)
            {
                return (str[0] == 'h' && str[1] == 'i' && str[2] == ' ');
            }
            else if (str.Length == 2)
            {
                return (str[0] == 'h' && str[1] == 'i');
            }
            return false;
        }

        public bool IcyHot(int a, int b)
        {
            return (a > 100 && b < 0)||(a < 0 && b > 100);
        }

        public bool Between10and20(int a, int b)
        {
            return (a <= 20 && a >= 10) || (b <= 20 && b >= 10);
        }

        public bool HasTeen(int a, int b, int c)
        {
            return (a <= 19 && a >= 13) || (b <= 19 && b >= 13) || (c <= 19 && c >= 13);
        }

        public bool SoAlone(int a, int b)
        {
            return !((a <= 19 && a >= 13) && (b <= 19 && b >= 13));
        }

        public string RemoveDel(string str)
        {
            if ((str.Length > 3) && (str[1] == 'd' && str[2] == 'e' && str[3] == 'l'))
            {
                return string.Format($"{str[0]}{str.Substring(4)}");
            }
            return str;
        }

        public bool IxStart(string str)
        {
            return str[1] == 'i' && str[2] == 'x';
        }

        public string StartOz(string str)
        {
            StringBuilder sb = new StringBuilder();
            if (str[0] == 'o')
            {
                sb.Insert(sb.Length, 'o');
            }
            if (str[1] == 'z')
            {
                sb.Insert(sb.Length, 'z');
            }
            return sb.ToString();
        }

        public int Max(int a, int b, int c)
        {
            if (a > b && a > c)
            {
                return a;
            }
            if (b>a && b>c)
            {
                return b;
            }
            return c;
        }

        public int Closer(int a, int b)
        {
            if ((Math.Abs(a - 10) == Math.Abs(b - 10)))
            {
                return 0;
            }
            if ((Math.Abs(a-10)>Math.Abs(b-10)))
            {
                return b;
            }
            return a;
        }

        public bool GotE(string str)
        {
            int sum = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == 'e')
                    sum++;
            }
            return sum >= 1 && sum <= 3;
        }





        public string EndUp(string str)
        {
            if (str.Length <= 3)
            {
                return str.ToUpper();
            }
            return string.Format($"{str.Substring(0, str.Length - 3)}{str.Substring(str.Length - 3).ToUpper()}");
        }

        public string EveryNth(string str, int n)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; str.Length > i; i+=n)
            {
                sb.Insert(sb.Length,str[i]);
            }
            return sb.ToString();

        }



    }
}

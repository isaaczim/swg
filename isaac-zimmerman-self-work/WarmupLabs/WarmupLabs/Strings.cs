﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarmupLabs
{
    public class Strings
    {
        public string SayHi(string name)
        {
            return string.Format($"Hello {name}!");
        }

        public string Abba(string a, string b)
        {
            string result = a + b + b + a;
            return result;
        }

        public string Tags(string tag, string content)
        {
            return string.Format($"<{tag}>{content}</{tag}>");
        }

        public string InsertWord(string container, string word)
        {
            string containerFront = container.Substring(0,2);
            string containerBack = container.Substring(2);
            return string.Format($"{containerFront}{word}{containerBack}");
        }

        public string MultipleEndings(string str)
        {
            string ending = str.Substring(str.Length - 2);
            return string.Format("{0}{0}{0}", ending);
        }

        public string FirstHalf(string str)
        {
            string half = str.Substring(0, str.Length/2);
            return half;
        }

        public string TrimOne(string str)
        {
            string newStr = str.Remove(str.Length - 1);
            return string.Format("{0}", newStr.Remove(0, 1));
        }

        public string LongInMiddle(string a, string b)
        {
            if (a.Length>b.Length)
            {
                return string.Format($"{b}{a}{b}");
            }
            return string.Format($"{a}{b}{a}");
        }

        public string RotateLeft2(string str)
        {
            string strFront = str.Substring(2);
            string strBack = str.Substring(0, 2);
            return string.Format($"{strFront}{strBack}");
        }

        public string RotateRight2(string str)
        {
            string strFront = str.Substring(str.Length - 2);
            string strBack = str.Substring(0, str.Length - 2);
            return string.Format($"{strFront}{strBack}");
        }

        public string TakeOne(string str, bool fromFront)
        {
            if (fromFront)
            {
                return string.Format("{0}", str.Substring(0, 1));
            }
            return string.Format("{0}", str.Substring(str.Length - 1, 1));

        }

        public string MiddleTwo(string str)
        {
            return string.Format("{0}{1}",str[(str.Length/2)-1], str[str.Length/2]);
        }

        public bool EndsWithLy(string str)
        {
            if (str.Substring(str.Length-2) == "ly")
            {
                return true;
            }
            return false;
        }

        public string FrontAndBack(string str, int n)
        {
            string frontStr = str.Substring(0, n);
            string backStr = str.Substring(str.Length - n);
            return string.Format($"{frontStr}{backStr}");
        }

        public string TakeTwoFromPosition(string str, int n)
        {
            if (n > str.Length-2)
            {
                return string.Format("{0}", str.Substring(0, 2));
            }
            return string.Format("{0}", str.Substring(n, 2));
        }

        public bool HasBad(string str)
        {
            if (str.Length > 3)
            {
                return str.Substring(0, 4).Contains("bad");
            }
            return false;
        }

        public string AtFirst(string str)
        {
            while (str.Length <= 1)
            {
                str = str.Insert(str.Length, "@");
        
            }
            return str.Substring(0,2);
        }

        public string LastChars(string a, string b)
        {
            if (a.Length == 0)
            {
                a = "@";
            }
            if (b.Length == 0)
            {
                b = "@";
            }
            return string.Format($"{a.Substring(0, 1)}{b.Substring(b.Length - 1)}");
        }

        public string ConCat(string a, string b)
        {
            if (a.Length == 0 || b.Length == 0)
            {
                return string.Format($"{a}{b}");
            }
            if (a[a.Length-1]==b[0])
            {
                a = a.Substring(0, a.Length - 1);
            }
            return string.Format($"{a}{b}");
        }

        public string SwapLast(string str)
        {
            string lastStr = str.Substring(str.Length - 1);
            string secondStr = str.Substring(str.Length - 2,1);
            string newStr = str.Substring(0, str.Length - 2);
            return string.Format($"{newStr}{lastStr}{secondStr}");
        }

        public bool FrontAgain(string str)
        {
            if (!string.IsNullOrEmpty(str) && str.Length >= 2)
            {
                return str.Substring(0, 2) == str.Substring(str.Length - 2);
            }
            return false;
        }

        public string MinCat(string a, string b)
        {
            if (a.Length > b.Length)
            {
                return string.Format($"{a.Substring(a.Length - b.Length)}{b}");
            }
            return string.Format($"{a}{b.Substring(b.Length - a.Length)}");
        }

        public string TweakFront(string str)
        {
            if (str.Length >= 2 && str[1] != 'b')
            {
                str = $"{str.Substring(0, 1)}{str.Substring(2)}";
            }
            if (str.Length > 1 && str[0] != 'a')
            {
                str = str.Substring(1);
            }
            return str;
        }

        public string StripX(string str)
        {
            if (str[0] == 'x')
            {
                str = str.Substring(1);
            }
            if (str[str.Length - 1] == 'x')
            {
                str = str.Substring(0, str.Length-1);
            }
            return str;
        }
    }
}

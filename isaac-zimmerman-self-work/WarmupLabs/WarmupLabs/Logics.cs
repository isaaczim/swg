﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace WarmupLabs
{
    public class Logics
    {
        public bool GreatParty(int n, bool a)
        {
            if (a)
                return n >= 40;
            
            return (n >= 40 && n <= 60);
        }

        public int CanHazTable(int a, int b)
        {
            if (a >= 8 || b >= 8)
                return 2;
            else if (a <= 2 || b <= 2)
                return 0;
            return 1;
        }

        public bool PlayOutside(int n, bool a)
        {
            if (a)
            {
                return n >= 70;
            }
            return n >= 70 && n <= 90;
        }

        public int CaughtSpeeding(int n, bool a)
        {
            if (a)
            {
                if (n<=65)
                    return 0;
                else if (n>=85)
                    return 2;
                return 1;
            }
            if (n >= 80)
                return 2;
            else if (n <= 60)
                return 0;
            return 1;
        }

        public int SkipSum(int a, int b)
        {
            if (a + b >= 10 && a + b <= 19)
                return 20;
            return a + b;
        }

        public string AlarmClock(int n, bool vac)
        {
            if (vac)
            {
                if (n > 0 && n < 6)
                    return "10:00";
                return "off";
            }
            if (n > 0 && n < 6)
                return "7:00";
            return "10:00";
        }

        public bool LoveSix(int a, int b)
        {
            return (a == 6 || b == 6 || a + b == 6 || a - b == 6);
        }

        public bool InRange(int a, bool b)
        {
            if (b)
                return a >= 10 || a <= 1;
            return a <= 10 && a >= 1;

        }

        public bool Special11(int n)
        {
            return n%11 == 0 || (n - 1)%11 == 0;
        }

        public bool Mod20(int n)
        {
            return (n - 2)% 20 == 0 || (n - 1) % 20 == 0;
        }

        public bool Mod35(int n)
        {
            if(n%3 == 0 && n%5 == 0)
                return false;
            return (n%3 == 0) || (n%5 == 0);
        }

        public bool AnswerCell(bool isMorning, bool isMom, bool isAsleep)
        {
            return (!isAsleep) && (!isMorning && !isMom);
        }

        public bool TwoIsOne(int a, int b, int c)
        {
            return a + b == c || a + c == b || b + c == a;
        }

        public bool AreInOrder(int a, int b, int c, bool bOK)
        {
            if (bOK)
            {
                return c > a;
            }
            return a < b && b < c;
        }

        public bool InOrderEqual(int a, int b, int c, bool equalOK)
        {
            if (equalOK)
            {
                return a <= b && b<= c;
            }
            return a < b && b < c;
        }

        public bool LastDigit(int a, int b, int c)
        {
            string _a = a.ToString();
            string _b = b.ToString();
            string _c = c.ToString();
            
            return ((_a[_a.Length-1] == _b[_b.Length - 1]) || (_a[_a.Length - 1] == _c[_c.Length - 1]) || (_c[_c.Length - 1] == _b[_b.Length - 1]));
        }

        public int RollDice(int die1, int die2, bool noDoubles)
        {
            if (noDoubles && die1 == die2)
            {
                if (die1 == 6)
                    return 7;
                return die1 + die2 + 1;
            }
            return die1 + die2;
        }
    }
}

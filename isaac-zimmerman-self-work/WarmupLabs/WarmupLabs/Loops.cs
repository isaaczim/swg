﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace WarmupLabs
{
    public class Loops
    {
        public string StringTimes(string str, int n)
        {
            string newStr = "";
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < n; i++)
            {
                sb.Insert(sb.Length, str);
            }
            return sb.ToString();
        }

        public string FrontTimes(string str, int n)
        {
            if (string.IsNullOrEmpty(str)) return string.Format("");

            StringBuilder sb = new StringBuilder();

            if (str.Length > 3)
            {
                str = str.Substring(0, 3);
            }
            else if (str.Length == 2)
            {
                str = str.Substring(0, 2);
            }
            else if (str.Length == 1)
            {
                str = str.Substring(0, 1);
            }
            for (int i = 0; i < n; i++)
            {
                sb.Insert(sb.Length, str);
            }
            return sb.ToString();
        }

        public int CountXX(string str)
        {
            int count = 0;
            for (int i = 0; i < str.Length-1; i++)
            {
                if (str.Substring(i, 2) == "xx") count++;
            }
            return count;
        }

        public bool DoubleX(string str)
        {
            int i = 0;
            char ch = 'y';
            while (i < str.Length-1 && ch == 'y')
            {
                if (str.Substring(i, 1) == "x" )
                {
                    if (str.Substring(i + 1,1)=="x")
                    {
                        return true;
                    }
                    return false;
                }
                i++;
            }
            return false;
        }

        public string EveryOther(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.Length; i+=2)
            {
                sb.Insert(sb.Length, str[i]);
            }
            return sb.ToString();
        }

        public string StringSplosion(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.Length; i++)
            {
                sb.Insert(sb.Length, str.Substring(0, i + 1));
            }
            return sb.ToString();
        }

        public int CountLast2(string str)
        {
            if (string.IsNullOrEmpty(str)) return 0;
            int count = 0;
      
            string strBack = str.Substring(str.Length - 2);

            for (int i = 0; i < str.Length-1; i++)
            {
                if (str.Substring(i, 2).Contains(strBack))
                {
                    count++;
                }
                
            }
            return count-1;
        }

        public int Count9(int[] arr)
        {
            int count = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == 9)
                {
                    count++;
                }
            }
            return count;
        }

        public bool ArrayFront9(int[] arr)
        {
            for (int i = 0; i < arr.Length && i < 4 ; i++ )
            {
                if (arr[i] == 9) return true;
            }
            return false;
        }

        public bool Array123(int[] numbers)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers.Length - i > 2)
                {
                    if (numbers[i] == 1 && numbers[i + 1] == 2 && numbers[i + 2] == 3)
                    {
                        return true;
                    }
                }
                
            }
            return false;
        }

        public int SubStringMatch(string a, string b)
        {
            int count = 0;
            if (a.Length>b.Length)
            {
                for (int i = 0; i < b.Length-1; i++)
                {
                    if (a[i] == b[i] && a[i+1] == b[i+1])
                    {
                        count++;
                    }
                }
            }
            else
            {
                for (int i = 0; i < a.Length-1; i++)
                {
                    if (a[i] == b[i] && a[i + 1] == b[i + 1])
                    {
                        count++;
                    }
                }
            }
            return count;

        }

        public string StringX(string str)
        {
            for (int i = 0; i < str.Length-1; i++)
            {
                if (i != 0 && str[i] == 'x' && i != str.Length)
                {
                    str = str.Remove(i,1);
                    i--;
                }
            }
            return str;
        }

        public string AltPairs(string str)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < str.Length; i++)
            {
                if (i == 0 || i == 1 || i == 4 || i == 5 || i == 8 || i == 9)
                {
                    sb.Insert(sb.Length, str[i]);
                }
            }
            return sb.ToString();
        }

        public string DoNotYak(string str)
        {
            string newStr = "";
            if (str.Length > 2)
            {
                for (int i = 0; i < str.Length && str.Length-i > 2; i++)
                {
                    if (str[i] == 'y' && str[i + 1] == 'a' && str[i+2] =='k')
                    {
                        newStr = newStr + str.Substring(0,i) + str.Substring(i + 3);
                    }
                }
                return newStr;
            }
            return str;
        }

        public int Array667(int[] numbers)
        {
            int count = 0;
            for (int i = 0; i < numbers.Length - 1; i++)
            {
                    if (numbers.Length >= 2 && numbers[i] == 6 && (numbers[i + 1] == 6 || numbers[i + 1] == 7))
                    {
                        count++;
                    }
            }
            return count;
        }

        public bool NoTriples(int[] numbers)
        {
            for (int i = 0; i < numbers.Length && numbers.Length-i > 2; i++)
            {
                if (numbers[i]==numbers[i+1] && numbers[i]==numbers[i+2])
                {
                    return false;
                }
            }
            return true;
        }

        public bool Pattern51(int[] numbers)
        {
            for (int i = 0; i < numbers.Length && numbers.Length - i > 2; i++)
            {
                if (numbers[i+1] == numbers[i] + 5 && numbers[i+2] == numbers[i]-1)
                {
                    return true;
                }
            }
            return false;
        }
    }
}

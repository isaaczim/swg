use Northwind
GO

SELECT *
from [Customers]

select *
from Customers
where Country = 'USA'

select * 
from Customers
where Country != 'USA'

select *
from Customers
where Country = 'USA' and Region = 'OR'

select *
from Customers
Where Country = 'USA' OR Country = 'UK'

select * 
from Customers
where Country in ('USA', 'UK')

select * 
from Customers
where Country not in ('USA', 'UK')


--comment one line

/*many lines of comment

see?

*/
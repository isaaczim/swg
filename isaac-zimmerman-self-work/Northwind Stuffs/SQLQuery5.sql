select city, lastname
from Employee
	right join location
	on Location.LocationID = Employee.LocationID

select city, lastname
from employee
	left join Location
	on location.LocationID = Employee.LocationID

select city, lastname
from employee
	full outer join Location
	on location.LocationID = Employee.LocationID
	
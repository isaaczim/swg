﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlipString
{
    class Program
    {
        static void Main(string[] args)
        {
            FlipString();
        }

        static void FlipString()
        {
            Console.Write("Enter String: ");
            string str = Console.ReadLine();
            for (int i = str.Length-1; i >= 0; i--)
            {
                Console.Write(str[i]);
            }
            Console.ReadKey();
        }
    }
}

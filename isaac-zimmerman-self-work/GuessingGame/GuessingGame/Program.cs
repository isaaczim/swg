﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame
{
    class Program
    {
        static void Main(string[] args)
        {
            string playerInput;
            int theAnswer;
            int playerInputInt;
            bool isNumberGuessed = false;
            int guesses = 0;

            Console.Write("Please enter username:");
            string user = Console.ReadLine();

            Random rng = new Random();
            theAnswer = rng.Next(1, 21);
            Console.Clear();
            
            Console.WriteLine($"{theAnswer}************* Guessing Game Press Q to Quit ***************");
            Console.WriteLine();
            do
            {
                //prompt user for number
                Console.Write($"Enter your guess {user} (1-20): ");
                playerInput = Console.ReadLine();
                //convert string to number
                if (int.TryParse(playerInput, out playerInputInt))
                {
                    if (playerInputInt < 1 || playerInputInt > 20)
                    {
                        Console.WriteLine("Please use a number between 1-20");
                    }
                    // check if user won
                    else if (playerInputInt == theAnswer)
                    {
                        if (guesses == 0)
                        {
                            Console.WriteLine("The First try? Press enter for a special prize");
                            Console.ReadLine();
                            Console.WriteLine(@"──────▄▀▄─────▄▀▄
─────▄█░░▀▀▀▀▀░░█▄
─▄▄──█░░░░░░░░░░░█──▄▄
█▄▄█─█░░▀░░┬░░▀░░█─█▄▄█");
                           break;

                        }
                        Console.WriteLine($"You Got It {user}!");
                        isNumberGuessed = true;
                    }
                    else
                    {
                        if (playerInputInt > theAnswer)
                            Console.WriteLine($"Too High {user}!");
                        else
                            Console.WriteLine($"Too Low {user}!");
                    }
                    guesses++;
                }
                else if (playerInput.ToUpper() == "Q")
                {
                    break;
                }
                else
                {
                    Console.WriteLine($"{user}! Don't You know what a number is? Try again.");
                }                

            } while (!isNumberGuessed);
            
            //Console.Clear();
            Console.WriteLine($"Press Any Key to Close Window.");
            Console.ReadKey();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ModelValidation1.Models.Annotations;

namespace ModelValidation1.Models
{
    //[NoGarfieldOneMonday(ErrorMessage = "Garfield cannot book on Mondays")]
    public class Appointment : IValidatableObject
    {
        [Display(Name = "Name")]
        [Required]
        public string ClientName { get; set; }

        [DataType(DataType.Date)]
        //[FutureDate(ErrorMessage = "Please enter a future date.")]
        public DateTime Date { get; set; }

        [Display(Name = "Terms and Agreements")]
        //[MustBeTrue(ErrorMessage = "You must aggree to the terms of service.")]
        public bool TermsAccepted { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (string.IsNullOrWhiteSpace(ClientName))
            {
                errors.Add(new ValidationResult("Please enter yor name", new[] {"ClientName"}));
            }
            if (DateTime.Now > Date)
            {
                errors.Add(new ValidationResult("Please enter a future date", new[] {"Date"}));
            }
            if (errors.Count == 0 && ClientName == "Garfield" && Date.DayOfWeek == DayOfWeek.Monday)
            {
                errors.Add(new ValidationResult("Garfield..."));
            }
            if (!TermsAccepted)
            {
                errors.Add(new ValidationResult("You must accept the terms and conditions"));
            }

            return errors;
        }
    }

}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Policy;
using System.Web;

namespace ModelValidation1.Models.Annotations
{
    public class NoGarfieldOneMondayAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            Appointment appt = value as Appointment;
            if (appt == null || string.IsNullOrWhiteSpace(appt.ClientName))
                return true;
            else
                return !(appt.ClientName == "Garfield" && appt.Date.DayOfWeek == DayOfWeek.Monday);
        }
    }
}
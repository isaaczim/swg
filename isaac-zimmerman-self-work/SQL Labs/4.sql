use Northwind
go

create view EmployeesAndManagers
as

select e1.EmployeeID, e1.LastName, e1.FirstName, e1.Title, 
e2.firstname + ' ' + e2.LastName as managerName, e2.Title as ManagerTitle
from Employees e1
	inner join Employees e2
	on e1.ReportsTo = e2.EmployeeID
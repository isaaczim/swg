use Northwind
go

select CompanyName
from customers c
full outer join orders o
on o.CustomerID = c.CustomerID
group by c.CompanyName
having (count(OrderID) > 2)

select (e.LastName + ' ' + e.FirstName) as EmployeeName,
 count(orderid) as OrdeCount
from Employees e
full outer join orders o
on o.EmployeeID = e.EmployeeID
group by e.FirstName, e.LastName
having (count(orderid) > 100)
select max(p.UnitPrice) as MaxPrice, CategoryName
from products p
full outer join Categories c
on c.CategoryID = p.CategoryID
group by c.CategoryName

select max(o.Freight + (od.UnitPrice*od.Quantity)) as orderTotal, 
c.CompanyName,
max(od.Quantity)
from orders o
full outer join customers c
on o.CustomerID = c.CustomerID
full outer join [Order Details] od
on o.OrderID = od.OrderID
group by c.CompanyName
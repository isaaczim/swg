Create procedure GetGrantsByEmployee
(
	@EmployeeLastName nchar(10)
)as

select e.FirstName, e.LastName, e.EmpID, g.GrantName, g.Amount
from Employee e
join [Grant] g
on e.EmpID = g.EmpID
where e.LastName = @EmployeeLastName
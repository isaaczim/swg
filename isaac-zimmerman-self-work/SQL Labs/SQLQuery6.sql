select *
from Employee e
where e.EmpID = 11

update Employee
set LastName = 'Green'
from Employee e
where e.EmpID = 11

select *
from Employee e
join Location l
on l.LocationID = e.LocationID
where l.City = 'Spokane'

update Employee
set Status = 'External'
from Employee e
join Location l
on l.LocationID = e.LocationID
where l.City = 'Spokane'

select *
from Employee e
join Location l
on l.LocationID = e.LocationID
where l.City = 'Seattle'

update Location
set Street = '111 1st Ave.'
from Location l
where l.City = 'Seattle'

select *
from Employee e
join Location l
on l.LocationID = e.LocationID
join [dbo].[Grant] g
on e.EmpID = g.EmpID
where l.City = 'Boston'

update [dbo].[Grant]
set Amount = 20000
from Employee e
join Location l
on l.LocationID = e.LocationID
join [dbo].[Grant] g
on e.EmpID = g.EmpID
where l.City = 'Boston'

select *
from MgmtTraining mt
where mt.ClassDurationHours > 20

delete mt
from MgmtTraining mt
where mt.ClassDurationHours > 20
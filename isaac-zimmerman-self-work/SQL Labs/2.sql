select (UnitPrice * p.UnitsInStock) as StockPrice
from Products p
order by StockPrice desc

select (LastName + FirstName) as NameLastFirst
from Employees e
order by LastName, FirstName

select (UnitPrice * p.UnitsInStock) as StockPriceUS, 
((UnitPrice * 1.3) * p.UnitsInStock) as Can, 
((UnitPrice * 106.8) * p.UnitsInStock) as yen, 
((UnitPrice * 0.88) * p.UnitsInStock) as euro, 
((UnitPrice * 18.59) * p.UnitsInStock) as Pesos 
from Products p
order by StockPriceUS desc
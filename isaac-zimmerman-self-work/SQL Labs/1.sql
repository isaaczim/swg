select *
from [grant] g
order by g.GrantName

select *
from Employee e
order by HireDate desc

select productname, Category, RetailPrice
from CurrentProducts p
order by RetailPrice desc

select *
from [grant] g
order by Amount desc, GrantName

select firstname, lastname, city
from Employee e full outer join Location l on l.LocationID = e.LocationID
order by City
use [master]
go

create login movieAppUser
	with password = '123456'

	go

use Northwind
go

create user movieuser for login movieAppUser
go

grant select on products to movieuser;
go
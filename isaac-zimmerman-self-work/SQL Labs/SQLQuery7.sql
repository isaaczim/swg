
Create procedure GetCustomerOrdersByDate
(
	@CustomerID nchar(5),
	@MinOrderDate datetime,
	@MaxOrderDate datetime
)as

select * 
from Customers
where CustomerID = @CustomerID

select * 
from Orders
where CustomerID = @CustomerID
	and orderdate between @MinOrderDate and @MaxOrderDate
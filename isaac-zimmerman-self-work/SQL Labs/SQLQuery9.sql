exec GetAllCustomers

declare @MyCustomerID nchar(5)
set @MyCustomerID = 'BOTTM'

exec GetCustomerOrdersByDate @MyCustomerID, '1/1/1997', '12/31/1997'

exec GetCustomerOrdersByDate  @MinOrderDate = '1/1/1997', @CustomerID = @MyCustomerID, @MaxOrderDate = '12/31/1997'
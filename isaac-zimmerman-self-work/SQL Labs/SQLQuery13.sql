Create procedure UpdateGrant
(
	@GrantID nchar(10),
	@Amount decimal,
	@GrantName nchar(10)
)as

update g
set Amount = @Amount
from [Grant] g
where g.GrantID = @GrantID

update g
set GrantName = @GrantName
from [Grant] g
where g.GrantID = @GrantID
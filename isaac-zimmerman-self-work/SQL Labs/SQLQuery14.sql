Create procedure UpdateGrantnew
(
	@GrantID nchar(10),
	@Amount decimal,
	@GrantName nchar(10),
	@EmpID int
)as

insert [Grant] (GrantID, GrantName, EmpID, Amount)
values (@GrantID, @GrantName, @EmpID, @Amount)
USE [SWCCorp]
GO

INSERT INTO [dbo].[Customer]
           ([CustomerID]
           ,[CustomerType]
           ,[FirstName]
           ,[LastName]
           ,[CompanyName])
     VALUES
           (2,'Consumer','Lee','Young',Null),
		   (3,'Consumer','Patricia','Martin',Null),
		   (4,'Consumer','Mary','Lopez',Null),
		   (5,'Business',null,null,'MoreTechnology.com')
GO



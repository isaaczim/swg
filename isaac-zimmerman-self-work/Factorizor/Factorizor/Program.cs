﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factorizor
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isNumber = false;
            int number;
            int sum = 0;
            string repeat = "y";

            do
            {
                //checks that input is a number
                do
                {
                    Console.Write("Please Enter A Number To Factorize: ");
                    string input = Console.ReadLine();
                    if (int.TryParse(input, out number))
                    {
                        isNumber = true;
                    }
                    else
                    {
                        Console.WriteLine("Please Enter A Valid Number");
                    }
                } while (!isNumber);

                Console.Clear();
                Console.WriteLine("********************* Factorizor ***********************\n");
                Console.WriteLine($"The Factors of {number} are:\n");

                //checks all factors of number
                for (int i = 1; i < number; i++)
                {
                    if (number%i == 0)
                    {
                        Console.WriteLine(i);
                        Console.WriteLine();
                        sum += i;
                    }
                }

                //checks if number is perfect
                if (sum == number)
                {
                    Console.WriteLine($"{number} is a perfect number\n");
                }
                else
                {
                    Console.WriteLine($"{number} is not a perfect number\n");
                }

                //checks if number is prime
                if (sum == 1)
                {
                    Console.WriteLine($"{number} is a prime number\n");
                }
                else
                {
                    Console.WriteLine($"{number} is not a prime number\n");
                }

                //check for re-run
                Console.Write("Enter 'y' to run again or 'n' to quit: ");
                repeat = Console.ReadLine();
                Console.Clear();

            } while (repeat == "y");
        }
    }
}

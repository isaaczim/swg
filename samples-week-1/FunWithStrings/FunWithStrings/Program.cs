﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FunWithStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("******** Fune With Strings ********");
            //BasicStringFunctionality();
            //StringConcatenation();
            //EscapeCharacer();
            //StringEquality();
            //StringsAreImmutable();
            StringBuilderExample();

            Console.ReadLine();
        }

        #region StringConcatenation
        static void StringConcatenation()
        {
            Console.WriteLine("=> String Concatenation");
            string s1 = "This is the first ";
            string s2 = "part of the string";
            string s3 = string.Concat(s1, s2);
            string s4 = s1 + s2;
            Console.WriteLine($"s4: {s4}");
            Console.WriteLine(s3);
            Console.ReadLine();
            Console.Clear();
        }
#endregion

        #region BasicStringFunctionality
        static void BasicStringFunctionality()
        {
            Console.WriteLine("=> Basic String Functionality");
            String firstName = "Freddy";
            Console.WriteLine($"Value of firstName {firstName}");
            Console.WriteLine("firstName has {0} characters.", firstName.Length);
            Console.WriteLine("firstName in uppercase: {0}", firstName.ToUpper());
            Console.WriteLine("firstName in lowercase: {0}", firstName.ToLower());
            Console.WriteLine("firstName contains the letter y? {0}", firstName.Contains("y"));
            Console.WriteLine("firstName after replace: {0}", firstName.Replace("dy",""));
            Console.ReadLine();
            Console.Clear();
        }
        #endregion

        #region EscapeCharacters

        static void EscapeCharacer()
        {
            Console.WriteLine("=> Escape characters:\a");
            string strWithTabs = "Model\tColor\tspeed\tPet Name\a";
            Console.WriteLine(strWithTabs);

            Console.WriteLine("Everyone loves \"Hello World\"");
            Console.WriteLine("C:\\MyApps\\bin\\Debug\\somefile.txt");

            Console.WriteLine(@"C:\MyApps\bin\Debug\somefile.txt");

            Console.WriteLine(@"deve wrote the ""Hello World"" app");

            Console.ReadLine();
        }
        #endregion

        #region StringEquality

        static void StringEquality()
        {
            Console.WriteLine("=> String Equality");
            string s1 = "Hello!";
            string s2 = "Yo";
            string s3 = "Yo";
            Console.WriteLine($"s1 = {s1}");
            Console.WriteLine($"s2 = {s2}");
            Console.WriteLine();

            Console.WriteLine($"s1 == s2: {s1 == s2}");
            Console.WriteLine($"s1 == Hello!: {s1 == "Hello!"}");
            Console.WriteLine($"s1 == HELLO!: {s1 == "HELLO!"}");
            Console.WriteLine($"s1.Equals(s2): {s1.Equals(s2)}");
            Console.WriteLine($"s2.Equals(s3): {s2.Equals(s3)}");
        }
        #endregion

        #region StringAre Immuable

        static void StringsAreImmutable()
        {
            string s1 = "This is my string";
            Console.WriteLine($"s1 = {s1}");

            string upperString = s1.ToUpper();
            Console.WriteLine($"upperString = {upperString}");

            Console.WriteLine($"s1 = {s1}");
        }
        #endregion

        #region StringBuilderExample

        static void StringBuilderExample()
        {
            Console.WriteLine("=> String Builder Example");
            Console.WriteLine();

            StringBuilder sb = new StringBuilder();

            sb.Append("List of movies");
            sb.Append("\nShawshand Redemption");
            sb.Append("\nGroundhod day");
            sb.Append("\nIronMan");
            sb.Append("\nStar Wars" +
                      "\nAvengers" +
                      "\nX-Men");
            Console.WriteLine(sb.ToString());
        }
        #endregion
    }
}

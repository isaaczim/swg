﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCalculatorKata
{
    public class Calculator
    {
        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }
            else if (numbers.Contains("//"))
            {
                string[] splitNumbers = numbers.Split(numbers[2]);
                int sum = 0;
                for (int i = 1; i < splitNumbers.Length; i++)
                {
                    if (int.Parse(splitNumbers[i]) < 1000)
                    {
                        sum += int.Parse(splitNumbers[i]);
                    }
                }
                return sum;
            }
            else if (numbers.Contains(","))
            {
                string[] splitNumbers = numbers.Split(',');
                int sum=0;
                for (int i = 0; i < splitNumbers.Length; i++)
                {
                    if (int.Parse(splitNumbers[i]) < 1000)
                    {
                        sum += int.Parse(splitNumbers[i]);
                    }
                }
                return sum;
            }
            else if (numbers.Contains("\n"))
            {
                string[] splitNumbers = numbers.Split('\n');
                int sum = 0;
                for (int i = 0; i < splitNumbers.Length; i++)
                {
                    if (int.Parse(splitNumbers[i]) < 1000)
                    {
                        sum += int.Parse(splitNumbers[i]);
                    }
                }
                return sum;
            }  
            return int.Parse(numbers);
        }
    }
}

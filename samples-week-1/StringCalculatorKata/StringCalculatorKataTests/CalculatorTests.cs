﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StringCalculatorKata;

namespace StringCalculatorKataTests
{
    [TestFixture]
    public class CalculatorTests
    {
        private Calculator _stringCalc;

        [SetUp]
        public void BeforeEachTest()
        {
            _stringCalc = new Calculator();
        }

        [Test]
        public void Add_EmtyStringReturnsZero()
        {
            //act
            int result = _stringCalc.Add("");

            //assert
            Assert.AreEqual(0,result);
        }

        [Test]
        public void Add_ReturnsNumberWithOneInput()
        {
            //act
            int result = _stringCalc.Add("5");

            //assert
            Assert.AreEqual(5,result);
        }

        [TestCase("1,2", 3)]
        [TestCase("2,3", 5)]
        [TestCase("10,20", 30)]
        public void Add_HandleTwoNumbers(string input, int expected)
        {
            int result = _stringCalc.Add(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase("1,2,3", 6)]
        [TestCase("10,20,30,40", 100)]
        [TestCase("1,1,1,1,1",5)]
        public void Add_HandleUnknownNumberOfArgs(string input, int expected)
        {
            int result = _stringCalc.Add(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase("1\n2", 3)]
        [TestCase("2\n3", 5)]
        [TestCase("10\n20", 30)]
        public void Add_HandleNewLine(string input, int expected)
        {
            int result = _stringCalc.Add(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase("//,\n2,3", 5)]
        [TestCase("//;\n10;20", 30)]
        [TestCase("// \n1 1 1 1 1", 5)]
        public void Add_HandleDelimter(string input, int expected)
        {
            int result = _stringCalc.Add(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase("1001,2", 2)]
        public void Add_HandleOver1000(string input, int expected)
        {
            int result = _stringCalc.Add(input);

            Assert.AreEqual(expected, result);
        }

        [TestCase("-1, 2", "Negatives not allowed: -1")]
        public void Add_NegativesNotAllowed(string input, string expected)
        {
            Assert.Throws<NotImplementedException>(_stringCalc.NegativesNotAllowed(input));
        }
    }
}

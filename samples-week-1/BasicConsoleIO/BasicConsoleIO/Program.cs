﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicConsoleIO
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("********* Basic Console I/O *********");
            Console.ReadLine();
            GetUserData();
            Console.ReadLine();
            Console.Clear();

//            Console.WriteLine("{0}, Number {0}, Number {0}", 9);

//            Console.WriteLine("{1}, {0}, {2}", 10, 20, 30);

            DemonstrateAlignemnt();

            Console.ReadLine();
            Console.Clear();

            FormatNumaricalData();

            Console.ReadLine();

        }

        static void DemonstrateAlignemnt()
        {
            LeftAlign();
            RigthAlign();
        }

        static void LeftAlign()
        {
            string lineOutputFormat = "{0,-15} {1,-2} {2, -5}";
            Console.WriteLine(lineOutputFormat, "John Doe", "OH", "44133");
            Console.WriteLine(lineOutputFormat, "Jane Doe", "NY", "12065");
            Console.WriteLine(lineOutputFormat, "Cheech Marin", "CA", "90210");
            Console.ReadLine();
            Console.Clear();

        }

        static void RigthAlign()
        {
            Console.WriteLine("Account Balances");
            string lineOutputFormat = "{0,-15} {1,13:C}";
            Console.WriteLine(lineOutputFormat, "A123456", 5302.327);
            Console.WriteLine(lineOutputFormat, "A23423442", 27.32);
            Console.WriteLine(lineOutputFormat, "A342423", 32706.00);
            Console.ReadLine();
        }

        static void FormatNumaricalData()
        {
            Console.WriteLine("the value of 99999 in various formats   ");
            Console.WriteLine("c format : {0:C}", 99999);
            Console.WriteLine("d9 format: {0:d9}", 99999);
            Console.WriteLine("f3 format: {0:f3}", 99999);
            Console.WriteLine("E format: {0:E}", 99999);
            Console.WriteLine("e format: {0:e}", 99999);
            Console.WriteLine("X format: {0:X}", 99999);
            Console.WriteLine("x format: {0:x}", 99999);
        }

        static void GetUserData()
        {
            //get name and age
            Console.Write("Please enter your name: ");
            string userName = Console.ReadLine();

            Console.Write("Please enter your age: ");
            string userAge = Console.ReadLine();

            //Change color
            ConsoleColor prevColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Yellow;

            Console.WriteLine("Hello {0}! You are {1} years old. Thanks {0}!", userName, userAge);

            //Restore previous color.
            Console.ForegroundColor = prevColor;
        }
    }
}
